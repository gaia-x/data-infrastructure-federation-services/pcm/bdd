/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package core;

import core.*;
import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

public class RequestContainer {
    private static RequestContainer instance = new RequestContainer();
    private List<Request> requestList = new ArrayList<>();

    private RequestContainer() {
    }

    public static RequestContainer getInstance() {
        return instance;
    }

    public void addRequest(Request request) {
        // Create new Request object, to differentiate from the currentRequest:
        Request reqToAdd = new Request(request);

        requestList.add(reqToAdd);

    }

    public Request getRequest(int index) {
        return requestList.get(index);
    }

    public Request getLastRequest() {
        if (!requestList.isEmpty()) {
            return getRequest(requestList.size() - 1);
        } else
            return null;
    }

    public void clearContainer() {
        requestList.clear();
    }

    public void printAllRequests() {
        System.out.println("Printing all requests in the RequestContainer ..... :\n");

        for (int i = 0; i < requestList.size(); i++) {
            System.out.println("Request[" + i + "]:\n");
            System.out.println("Object ref= " + ObjectUtils.identityToString(requestList.get(i)));
            System.out.println(requestList.get(i));

        }
    }
}
