/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest;

import api.test.rest.pojo.Member;

public class RestSessionContainer {
    private Member member;
    private String oauthToken;
    private String oauthUUID;
    private String personEntityUUID;
    private String organizationEntityUUID;
    private String assetEntityUUID;
    private String passportEntityUUID;
    private String documentEntityUUID;
    private String conversationEntityUUID;
    private String vcardURL;
    private String wopiAccessToken;
    private String qrCode;
    private String actionID;
    private String publicKey;
    private String carddavUsername;
    private String carddavPassword;
    private String identificator;
    private String newDevicePublicKey;
    private String deviceID;
    private String phoneNumber;
    private String currentCompany;
    private String recipientEmail;
    private String messegeId;
    private String subject;
    private String batchKey;
    private String batchKey2;
    private String batchValue;

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    public Member getCurrentMember() {
        return member;
    }

    public void setCurrentMember(Member member) {
        this.member = member;
    }

    public String getOauthToken() {
        return oauthToken;
    }

    public void setOauthToken(String oauthToken) {
        this.oauthToken = oauthToken;
    }

    public String getOauthUUID() {
        return oauthUUID;
    }

    public void setOauthUUID(String oauthUUID) {
        this.oauthUUID = oauthUUID;
    }

    public String getPersonEntityUUID() {
        return personEntityUUID;
    }

    public void setPersonEntityUUID(String personEntityUUID) {
        this.personEntityUUID = personEntityUUID;
    }

    public String getOrganizationEntityUUID() {
        return organizationEntityUUID;
    }

    public void setOrganizationEntityUUID(String organizationEntityUUID) {
        this.organizationEntityUUID = organizationEntityUUID;
    }

    public String getAssetEntityUUID() {
        return assetEntityUUID;
    }

    public void setAssetEntityUUID(String assetEntityUUID) {
        this.assetEntityUUID = assetEntityUUID;
    }

    public String getPassportEntityUUID() {
        return passportEntityUUID;
    }

    public void setPassportEntityUUID(String passportEntityUUID) {
        this.passportEntityUUID = passportEntityUUID;
    }

    public String getDocumentEntityUUID() {
        return documentEntityUUID;
    }

    public void setDocumentEntityUUID(String documentEntityUUID) {
        this.documentEntityUUID = documentEntityUUID;
    }

    public String getConversationEntityUUID() {
        return conversationEntityUUID;
    }

    public void setConversationEntityUUID(String conversationEntityUUID) {
        this.conversationEntityUUID = conversationEntityUUID;
    }

    public String getVcardURL() {
        return vcardURL;
    }

    public void setVcardURL(String vcardURL) {
        this.vcardURL = vcardURL;
    }

    public String getWopiAccessToken() {
        return wopiAccessToken;
    }

    public void setWopiAccessToken(String wopiAccessToken) {
        this.wopiAccessToken = wopiAccessToken;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getActionID() {
        return actionID;
    }

    public void setActionID(String actionID) {
        this.actionID = actionID;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getCarddavUsername() {
        return carddavUsername;
    }

    public void setCarddavUsername(String carddavUsername) {
        this.carddavUsername = carddavUsername;
    }

    public String getCarddavPassword() {
        return carddavPassword;
    }

    public void setCarddavPassword(String carddavPassword) {
        this.carddavPassword = carddavPassword;
    }

    public String getNewDevicePublicKey() {
        return newDevicePublicKey;
    }

    public void setNewDevicePublicKey(String newDevicePublicKey) {
        this.newDevicePublicKey = newDevicePublicKey;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        this.currentCompany = currentCompany;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getMessegeId() {
        return messegeId;
    }

    public void setMessegeId(String messegeId) {
        this.messegeId = messegeId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBatchKey() {
        return batchKey;
    }

    public void setBatchKey(String batchKey) {
        this.batchKey = batchKey;
    }

    public String getBatchValue() {
        return batchValue;
    }

    public void setBatchValue(String batchValue) {
        this.batchValue = batchValue;
    }

    public String getBatchKey2() {
        return batchKey2;
    }

    public void setBatchKey2(String batchKey2) {
        this.batchKey2 = batchKey2;
    }
}

