#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @dashboard @all @wip
Feature: Activity - Profiles

  @activity
  Scenario: Edit the name of default and newly created passport and validate the Activity Page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
    And User click on Profiles tab
#Social passport opened - Edit Default Passport
    And User click on profile with name "Social"
    And User click on edit profile button
    And User populates profile name field with value "SocialEdited" and Save
#Validate the edited passport
    Then Confirmation Message is presented with text "The name of this passport has been updated to SocialEdited" and message disappears
#Validate the renaming of the default passport in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Create new passport
    When User click on Profiles tab
    When User creates new profile with name "Test"
    And User click on profile with name "Test"
#Edit the passport
    And User click on edit profile button
    And User populates profile name field with value "TestEdited" and Save
#Validate the edited passport
    Then Confirmation Message is presented with text "The name of this passport has been updated to TestEdited" and message disappears
#Validate the renaming of the newly created passport in the Activity Page
    When User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Add claims to newly created passport and validate them in the Activity Page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
#Adding new claim to the identity
    And User click on Identity tab
    And User select "Language" option from main Identity select
    And User select "Bulgarian" from the Language select
    And User click on Add to your Identity button
    And User click on Profiles tab
#Create new Passport
    And User creates new profile with name "Local"
#Validate the passports are 4
    Then Validate "4" profiles are presented
#Validate the newly created passport is presented
    And Validate profile with name "Local" is presented
#Validate the newly created passport in the Activity Page
    When User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Click on Passports tab
    When User click on Profiles tab
    When User click on profile with name "Local"
#Add the claims to the passport and validate them
    And User add the "Language" claim to the profile
    Then Validate claim "Language" is presented under the profile
    #Validate the value of the Language
    When User add the "Email" claim to the profile
    Then Validate claim "Email" is presented under the profile
    When User add the "Full  name" claim to the profile
    Then Validate claim "Full  name" is presented under the profile
    When User add the "Phone" claim to the profile
    Then Validate claim "Phone" is presented under the profile
#Click on the Activity tab
    When User click on Activity tab
#Validate adding of the phone to the passport in the Activity Page
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Validate adding of the Full name claim to the passport in the Activity Page
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key Login from has value "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key Device key has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
#Validate adding the email claim to the passport in the Activity Page
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
#Validate adding of the Language claim to the passport in the Activity Page
    Then Validate the title of the "4" activity is "<string>"
    And Validate "4" activity with key Login from has value "<string>"
    And Validate "4" activity with key UUID has value "<string>"
    And Validate "4" activity with key Device key has value "<string>"
    And Validate "4" activity with key User entity UUID has value "<string>"
    And I close the browser

    @activity
    Scenario: Remove claims from passport and validate Activity Page
      Given I open Chrome browser and navigate to vereign login page
      And I register a new user with email via VIAM API
      And I login with the registered via the VIAM API user into dashboard
#Logged in
      When User click on close tour button
#Adding the claim to the identity
      And User click on Identity tab
      And User select "Language" option from main Identity select
      And User select "Bulgarian" from the Language select
      And User click on Add to your Identity button
      And User click on Profiles tab
      And User click on profile with name "Social"
#Validate the Language claim is presented in the Identity tab
      Then Validate Identity claim with label "Language" has value "Bulgarian"
##Add the claim to the passport
      When User add the "Language" claim to the profile
#Validate that the claim is added to the passport
      Then Validate Profile claim with label "Language" has value "Bulgarian"
      And User add the "Language" claim to the identity
      And Validate "Language" claim is deleted from the Profile page
#Validate the removal of the Language claim from the passport in the Activity Page
     And User click on Activity tab
      Then Validate the title of the "1" activity is "<string>"
      And Validate "1" activity with key Login from has value "<string>"
      And Validate "1" activity with key UUID has value "<string>"
      And Validate "1" activity with key Device key has value "<string>"
      And Validate "1" activity with key User entity UUID has value "<string>"
