#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard @wip
Feature: Activity - Device Manager

  @activity
  Scenario: Rename a device and validate the activity page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Logged in
    And User click on device manager button
#Rename Device
    And User hovers over the options button on the first device
    And User click on Rename device button
    And User populates the new device name "Device edited" and Save
#Click on Activity tab
    And User click on Activity tab
#Validate the Rename action in the acitivity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Validate Activity page after registration
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on Activity tab
#Validate the first item in the Activity Page
    Then Validate the title of the "1" activity is "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Validate the second item in the Activity Page
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
#Validate the third item in the Activity Page
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
#Validate the fourth item in the Activity Page
    Then Validate the title of the "3" activity is "<string>"
    And Validate "4" activity with key UUID has value "<string>"
    And Validate "4" activity with key User entity UUID has value "<string>"

