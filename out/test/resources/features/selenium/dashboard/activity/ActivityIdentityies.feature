#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard @wip
Feature: Activity - Identity

  @activity
  Scenario: Add claims to the identity and validate them in the activity page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Adding Full Name
    And User select "Name" option from main Identity select
    And User select "Full name" option from secondary Identity select
    And User populates the field with placeholder "First name" with value "Ivan"
    And User populates the field with placeholder "Last name" with value "Petrov"
    And User click on Add to your Identity button
#Validate the adding of the full name claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Nickname
    When User click on Identity tab
    And User select "Name" option from main Identity select
    And User select "Nickname" option from secondary Identity select
    And User populates the field with placeholder "Type your nickname here" with value "myNickname"
    And User click on Add to your Identity button
#Validate the adding of the nickname in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Date of Birth
    When User click on Identity tab
    And User select "Date of Birth" option from main Identity select
    And User populates the date of birth "01/01/2000"
    And User click on Add to your Identity button
#Validate the adding of the Date of Birth in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Gender
    When User click on Identity tab
    And User select random Gender from the select
    And User click on Add to your Identity button
#Validate the adding of the Gender in the Activity tab
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Address
    When User click on Identity tab
    And User select "Address" option from main Identity select
    And User populates the field with placeholder "Street address" with value "G.M Dimitrov"
    And User populates the field with placeholder "Locality" with value "Sofia"
    And User populates the field with placeholder "Region" with value "Sofia"
    And User populates the field with placeholder "Postal code" with value "1777"
    And User populates the field with placeholder "Country" with value "Bulgaria"
    And User click on Add to your Identity button
#Validate the adding of the address in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding email
    When User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Email" option from secondary Identity select
    And User populates the field with placeholder "Type your email here" with value "tested23@abv.bg"
    And User click on Add to your Identity button
#Validate the adding of the email in the Activity page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Phone
    When User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Phone" option from secondary Identity select
    And User populates the field with placeholder "Type your phone number here" with value "+359883353610"
    And User click on Add to your Identity button
#Validate the adding of the phone in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Twitter
    When User click on Identity tab
    And User select "Social" option from main Identity select
    And User select "Twitter" option from secondary Identity select
    And User populates the field with placeholder "Url" with value "www.localhost.com"
    And User click on Add to your Identity button
#Validate the adding of the twitter in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding LinkedIn
    And User click on Identity tab
    And User select "Social" option from main Identity select
    And User select "LinkedIn" option from secondary Identity select
    And User populates the field with placeholder "Url" with value "www.localhost.com"
    And User click on Add to your Identity button
#Validate the adding of the LinkedIn in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding Language
    When User click on Identity tab
    And User select random Language from the select
    And User click on Add to your Identity button
#Validate the adding of the Language in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Adding image claim to the identity
    When User click on Identity tab
    And User upload a photo with valid format to the identity
    Then Validate image is presented
#Validate the adding of the Image claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#TODO: Check if i have to validate only the first item in the activity page or more after adding the image claim to the identity
    And I close the browser

  @activity
  Scenario: Edit identity claims and validate the Activity Page
    Given I open Chrome browser and navigate to vereign login page
#Login and Add claims to the identity
    And I register a new user with email and add all claims via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
#Expand all folders
    And User expand all folders
#Edit the Address claim
    When User click on edit "Address" claim button
    And User populates the field with placeholder "Street address" with value "Street Address Edited"
    And User populates the field with placeholder "Locality" with value "Locality Edited"
    And User populates the field with placeholder "Region" with value "Region Edited"
    And User populates the field with placeholder "Postal code" with value "Code Edited"
    And User populates the field with placeholder "Country" with value "Country Edited"
    And User click on Update claim button
#Validate the Edit of the Address claim in the activity page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Chat claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Chat" claim button
    And User populates the field with placeholder "Chat Service" with value "Chat Service Edited"
    And User populates the field with placeholder "Number or Username" with value "Username Edited"
    And User click on Update claim button
#Validate the Edit of the Chat claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Date of Birth claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Date of  birth" claim button
    And User populates the date of birth "02/02/2002"
    And User click on Update claim button
#Validate the Edit of the Date of Birth claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Email claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Email" claim button
    And User populates the field with placeholder "Type your email here" with value "edited@example.com"
    And User click on Update claim button
#Validate the edit of the Email claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Gender claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Gender" claim button
    And User select random Gender from the select
    And User click on Update claim button
#Validate the Edit of the Gender claim in the Activity page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Language claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Language" claim button
    And User select "Bulgarian" from the Language select
    And User click on Update claim button
#Validate the Edit of the Language claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Full Name claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Full  name" claim button
    And User populates the field with placeholder "First name" with value "First Name Edited"
    And User populates the field with placeholder "Last name" with value "Last Name Edited"
    And User click on Update claim button
#Validate the Edit of the Full name claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit nickname claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Nickname" claim button
    And User populates the field with placeholder "Type your nickname here" with value "Nickname Edited"
    And User click on Update claim button
#Validate the edit of the nickname claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Phone claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Phone" claim button
    And User populates the field with placeholder "Type your phone number here" with value "+359884045187"
    And User click on Update claim button
#Validate the edit of the phone claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Edit Social claim
    When User click on Identity tab
    And User expand all folders
    When User click on edit "Social" claim button
    And User populates the field with placeholder "Url" with value "www.localhost.com/edited"
    And User click on Update claim button
#Validate the edit of the Social claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Delete identity claims and validate Activity Page
    Given I open Chrome browser and navigate to vereign login page
#Login and add claims to the identity
    And I register a new user with email and add all claims via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
#Expand all folders
    And User expand all folders
#Delete Address Claim
    And User click on delete "Address" claim button
    And User confirms the deletion
#Validate the deletion of the Address claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Chat claim
    When User click on Identity tab
    And User expand all folders
    When User click on delete "Chat" claim button
    And User confirms the deletion
#Validate the deletion of the chat claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Date of birth claim
    When User click on Identity tab
    And User expand all folders
    When User click on delete "Date of  birth" claim button
    And User confirms the deletion
#Validate the deletion of the Date of birth claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Email claim
    When User click on Identity tab
    And User expand all main folders
    And User click on delete "Email" claim button
    And User confirms the deletion
#Validate the deletion of the email claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Gender claim
    When User click on Identity tab
    And User expand all folders
    And User click on delete "Gender" claim button
    And User confirms the deletion
#Validate the deletion of the Gender claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Language Claim
    When User click on Identity tab
    And User expand all folders
    And User click on delete "Language" claim button
    And User confirms the deletion
#Validate the deletion of the Language claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Full Name Claim
    When User click on Identity tab
    And User expand all main folders
    And User click on delete "Full  name" claim button
    And User confirms the deletion
#Validate the deletion of the Full name claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Nickname Claim
    When User click on Identity tab
    And User expand all folders
    And User click on delete "Nickname" claim button
    And User confirms the deletion
#Validate the deletion of the nickname claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Phone Claim
    When User click on Identity tab
    And User expand all main folders
    And User click on delete "Phone" claim button
    And User confirms the deletion
#Validate the deletion of the Phone claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Delete Social Claim
    When User click on Identity tab
    And User expand all folders
    And User click on delete "Social" claim button
    And User confirms the deletion
#Validate the deletion of the Social claim in the Activity Page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Validate email and phone claim and validate the Activity Page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
#Logged in
    And User click on Identity tab
#Edit Email
    And User click on edit "Email" claim button
    And User populates the Identity Email field with random valid email
    And User click on Update claim button
#Email is not validated after the edit
    Then Validate email field is not validated in the Identity Page
#Validate the email
    When User click on validate "Email" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Validate the verification of the email in the Activity Page
    And User click on Activity tab
    Then Validate the title of the "1" activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Validate the request for the code in the Activity Page
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key Login from has value "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key Device key has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
#Edit the phone
    And User click on Identity tab
    When User click on edit "Phone" claim button
    And User populates the Phone field with random value
    And User click on Update claim button
    Then Validate phone field is not validated in the Identity Page
#Validate the phone
    When User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Validate the verification of the phone in the Activity Page
    And User click on Activity tab
    Then Validate the title of the "1" activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
#Validate the request for the code in the Activity Page
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key Login from has value "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key Device key has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
    And I close the browser