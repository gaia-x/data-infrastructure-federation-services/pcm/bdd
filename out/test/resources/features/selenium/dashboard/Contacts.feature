#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard @notParallel
Feature: Dashboard - Contacts

  Background:
    Given we are testing the VIAM Api

  @contact @gmail @bug-dashboard-659
  Scenario Outline: Send verified gmail email from profile <Profile_Name> and validate the contact
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if the gmail account is created and login with gmail account
    And User logs into vereign with gmail account
#Change the environment
    When I change the extension environment
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User selects "<Profile_Name>" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "<Message>"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Validate the contact is not presented in the list of the sender
    And User navigates to vereign login page
    And User click on Contacts tab
    And Validate the contact is not presented
#Register the not registered user
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "<Contact_Name>"
    And User Validate the email of the "1" contact is "<Contact_Email>"
    And User Validate the phone of the "1" contact is "<Contact_Phone>"
    And User Validate the last updated of the "1" contact is "<Contact_Last_Updated>"
    And User Validate the email claim on the "1" row is "<Email_Verified>"
    And User Validate the phone claim on the "1" row is "<Phone_Verified>"
#Validate the contact is not presented in the contact list of the sender
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User click on Contacts tab
    And User scrolls to the botton of the page
#bug-dashboard-659
#    And Validate the contact is not presented
    And I close the browser

    Examples:
      | Profile_Name      | Message                              | Contact_Name       | Contact_Email                | Contact_Phone | Contact_Last_Updated | Email_Verified | Phone_Verified |
      | No Claims         | Email from profile No Claims         | Anonymous          | N/A                          | N/A           | Today                | not verified   | not verified   |
      | Name Only         | Email from profile Name Only         | Automation Vereign | N/A                          | N/A           | Today                | not verified   | not verified   |
      | Email             | Email from profile Email             | Automation Vereign | vereign.automation@gmail.com | N/A           | Today                | verified       | not verified   |
      | Friends           | Email from profile Friends           | Automation Vereign | vereign.automation@gmail.com | +359884045187 | Today                | verified       | not verified   |
      | Social            | Email from profile Social            | Automation Vereign | vereign.automation@gmail.com | +359884045187 | Today                | verified       | not verified   |
      | NotValidatedEmail | Email from profile NotValidatedEmail | Anonymous          | notValidated@example.com     | N/A           | Today                | not verified   | not verified   |


  @contacts @roundcube @bug-dashboard-659
  Scenario Outline: Send verified roundcube email from profile <Profile_Name> and validate the contact
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the roundcube account is created and login with roundcube account
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
    And User selects "<Profile_Name>" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field with random email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "<Message>"
    And User click on roundcube Send button
#Validate the contact is not presented in the list of the sender
    And User navigates to vereign login page
    And User click on Continue button
    And User click on Contacts tab
    And Validate the contact is not presented
#Register the not registered user
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "<Contact_Name>"
    And User Validate the email of the "1" contact is "<Contact_Email>"
    And User Validate the phone of the "1" contact is "<Contact_Phone>"
    And User Validate the last updated of the "1" contact is "<Contact_Last_Updated>"
    And User Validate the email claim on the "1" row is "<Email_Verified>"
    And User Validate the phone claim on the "1" row is "<Phone_Verified>"
#Validate the contact is not presented in the contact list of the sender
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
    And User click on Contacts tab
#bug-dashboard-659
#    And Validate the contact is not presented
    And I close the browser

    Examples:
      | Profile_Name | Message                      | Contact_Name    | Contact_Email                    | Contact_Phone | Contact_Last_Updated | Email_Verified | Phone_Verified |
      | No Claims    | Email from profile No Claims | Anonymous       | N/A                              | N/A           | Today                | not verified   | not verified   |
#      | Name Only         | Email from profile Name Only         | Robot Roundcube | N/A                              | N/A           | Today                | not verified   | not verified   |
      | Email        | Email from profile Email     | Robot Roundcube | mister.test@kolab-qa.vereign.com | N/A           | Today                | verified       | not verified   |
#      | Friends           | Email from profile Friends           | Robot Roundcube | mister.test@kolab-qa.vereign.com | +359884045187 | Today                | verified       | not verified   |
#      | Social            | Email from profile Social            | Robot Roundcube | mister.test@kolab-qa.vereign.com | +359884045187 | Today                | verified       | not verified   |
#      | NotValidatedEmail | Email from profile NotValidatedEmail | Anonymous       | notValidated@example.com         | N/A           | Today                | not verified   | not verified   |


  @contact @gmail @bug-dashboard-659
  Scenario: Send two gmail emails from different profiles and validate the contact
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the gmail account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User selects "No Claims" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail: Validate contact update"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Register the not registered user
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Anonymous"
    And User Validate the email of the "1" contact is "N/A"
    And User Validate the phone of the "1" contact is "N/A"
    And User Validate the last updated of the "1" contact is "Today"
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send an email to the same receiver from Social profile
    And User navigates to gmail
    And User selects "Friends" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with the same email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail: Validate contact update"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "vereign.automation@gmail.com"
    And User Validate the phone of the "1" contact is "+359884045187"
    And User Validate the last updated of the "1" contact is "Today"
    And User Validate the email claim on the "1" row is "verified"
    And User Validate the phone claim on the "1" row is "not verified"
    And User click on contact with name "Automation Vereign"
    And Validate Contact claim with label "Email" has value "vereign.automation@gmail.com"
    And Validate Contact claim with label "Full  name" has value "Automation, Vereign"
    And Validate Contact claim with label "Phone" has value "+359884045187"
    And Validate Contact title is "Automation Vereign"
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the contact of the receiver is not presented
    And User click on Contacts tab
    And User scrolls to the botton of the page
#bug-dashboard-659
#    And Validate the contact is not presented
    And I close the browser

  @contact @roundcube @bug-dashboard-659
  Scenario: Send two roundcube emails from different profiles and validate the contact
      #Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
#Check if the roundcube account is created and login with roundcube account
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
    And User selects "No Claims" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field with random email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube: Contact Update"
    And User click on roundcube Send button
#Register the not registered user
    And User navigates to vereign login page
    And User click on Continue button
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Anonymous"
    And User Validate the email of the "1" contact is "N/A"
    And User Validate the phone of the "1" contact is "N/A"
    And User Validate the last updated of the "1" contact is "Today"
#Logout and login with the roundcube account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And I wait for {5000} mseconds
#Enable vereign
    And User enables Vereign On
    And User selects "Friends" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To field with the same email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube: Contact Update"
    And User click on roundcube Send button
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Robot Roundcube"
    And User Validate the email of the "1" contact is "mister.test@kolab-qa.vereign.com"
    And User Validate the phone of the "1" contact is "+359884045187"
    And User Validate the last updated of the "1" contact is "Today"
    And User Validate the email claim on the "1" row is "verified"
    And User Validate the phone claim on the "1" row is "not verified"
#Logout and login with the roundcube account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the contact of the receiver is not presented
    And User click on Contacts tab
#bug-dashboard-659
#    And Validate the contact is not presented
#Log into roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And I wait for {5000} mseconds
#Enable vereign
    And User enables Vereign On
    And User selects "NotValidatedEmail" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To field with the same email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube: Contact Update"
    And User click on roundcube Send button
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Robot Roundcube"
    And User Validate the email of the "1" contact is "mister.test@kolab-qa.vereign.com"
    And User Validate the phone of the "1" contact is "+359884045187"
    And User Validate the last updated of the "1" contact is "Today"
    And User Validate the email claim on the "1" row is "verified"
    And User Validate the phone claim on the "1" row is "not verified"
    And User click on contact with name "Robot Roundcube"
    And Validate Contact claim with label "Email" has value "notValidated@example.com"
    And Validate Contact claim with label "Full  name" has value "Robot, Roundcube"
    And Validate Contact claim with label "Phone" has value "+359884045187"
    And Validate Contact title is "Robot Roundcube"
    And I close the browser

  @contact @gmail
  Scenario: Add and remove claim to the profile and validate the contact
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the gmail account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User selects "Contacts" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Edit profile and validate the contact"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Register the not registered user
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the contacts table
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "N/A"
    And User Validate the phone of the "1" contact is "N/A"
    And User Validate the last updated of the "1" contact is "Today"
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User click on close tour button
    And User click on Profiles tab
    And User click on profile with name "Contacts"
    And User add the "Phone" claim to the profile
    And Validate claim "Phone" is presented under the profile
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
#Validate the contact after the edit of the profile
    And User click on Contacts tab
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "N/A"
    And User Validate the phone of the "1" contact is "+359884045187"
    And User Validate the last updated of the "1" contact is "Today"
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User click on Profiles tab
    And User click on close tour button
    And User click on profile with name "Contacts"
    And User add the "Phone" claim to the identity
    And Validate "Phone" claim is deleted from the Profile page
#Logout and login with the newly created account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
#Validate the Phone claim in not removed from the contacts of the receiver
    And User click on Contacts tab
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "N/A"
    And User Validate the phone of the "1" contact is "+359884045187"
    And User Validate the last updated of the "1" contact is "Today"
    And User click on contact with name "Automation Vereign"
    And Validate Contact claim with label "Full  name" has value "Automation, Vereign"
    And Validate Contact claim with label "Phone" has value "+359884045187"
    And Validate Contact claim with label "Date of  birth" has value "Jan 01 2019"
    And Validate Contact title is "Automation Vereign"
    And I close the browser

  @contacts @gmail @search
  Scenario: Archive contact and validate it is not presented in the Contacts Page
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if the gmail account is created and login with gmail account
    And User logs into vereign with gmail account
#Change the environment
    When I change the extension environment
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Archive contact - validate it is not presented"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the name of the contact
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
#Archive the contact
    And User open the "1" contact
    And User click on Archive button
#Validate the message that the contact is archived
    And Confirmation Message is presented with text "Contact status has been updated successfully" and message disappears
    And Validate contact is archived meesage is presented
#Validate it is not presented in the Contacts Page
    And User click on Contacts tab
    And User Validates there is "0" contact presented
#Validate the Contacts is visible after a search
    And User populates contacts search field with value "Vereign"
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "vereign.automation@gmail.com"
#Un-archive contact
    And User open the "1" contact
    And User click on Un-archive button
    And Confirmation Message is presented with text "Contact status has been updated successfully" and message disappears
#Validaate the contact is presented in the Contacts Page without search
    And User click on Contacts tab
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
    And User Validate the email of the "1" contact is "vereign.automation@gmail.com"
    And I close the browser

  @gmail @contacts @search @bug-dashboard-677
  Scenario: Archive contact and validate it is presented after search
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if the gmail account is created and login with gmail account
    And User logs into vereign with gmail account
#Change the environment
    When I change the extension environment
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Archive contact - validate it is presented"
    And User click on gmail Send button
    And I wait for {2000} mseconds
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
    And User click on Contacts tab
#Validate the name of the contact
    And User Validates there is "1" contact presented
    And User Validate the name of the "1" contact is "Automation Vereign"
#Archive the contact
    And User open the "1" contact
    And User click on Archive button
#Validate the message that the contact is archived
    And Confirmation Message is presented with text "Contact status has been updated successfully" and message disappears
    And Validate contact is archived meesage is presented
#Validate it is not presented in the Contacts Page
    And User click on Contacts tab
    And User Validates there is "0" contact presented
    And User populates contacts search field with value "Vereign Automation"
#bug-dashboard-677
#    And User Validates there is "1" contact presented
    And I close the browser


