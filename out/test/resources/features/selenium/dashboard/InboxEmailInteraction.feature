#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @dashboard @all @notParallel
Feature: Dashboard - Inbox email interaction

  Background:
    Given we are testing the VIAM Api

  @inbox @gmail
  Scenario: Send verified gmail email to registered user
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#    When User logs into gmail
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to registered user"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Email
    And Validate the subject of the first email is the one used to send email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email to registered user"
    Then Validate the message of the email for registered user has value "Verified gmail email to registered user"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "Boris Dimitrov <tested23@abv.bg>"
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And Validate Name field in the email template has value "Vereign Automation"
    And Validate Email field in the email template has value "vereign.automation@gmail.com"
    And Validate verified icon is presented on the Email claim of the Email Thread
    And Validate profile image is presented on the V-card
    And I close the browser

  @inbox @gmail @bug-dashboard-704
  Scenario Outline: Send verified gmail email to registered user with changed profile
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User selects "<Profile_Name>" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "<Message>"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "<Profile_Name>"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "<Message>"
    Then Validate the message of the email for registered user has value "<Message>"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
#bug-dashboard-704
#    And Validate the profile in the email has value "<Profile_Name>"
    And Validate the date and time in the email has correct value
    And Validate small footer text "<Expected_Small_Footer_Text>"
    And Validate To value in the email thread has value "Boris Dimitrov <tested23@abv.bg>"
    And Validate From value in the email thread has value "<Expected_From_Value>"
    And I close the browser
    Examples:
      | Profile_Name | Message                                                                       | Expected_Small_Footer_Text                                                                          | Expected_From_Value                               |
      | No Claims    | Verified gmail email to registered user with profile that has no claims       | This email has been sent by a verified user and authenticated using the beta version of Vereign.    | <vereign.automation@gmail.com>                    |
      | Name Only    | Verified gmail email to registered user with profile that has Name only claim | This email has been sent by Vereign Automation and authenticated using the beta version of Vereign. | Vereign Automation <vereign.automation@gmail.com> |


  @inbox @gmail @bug-dashboard-704
  Scenario: Send verified gmail email to registered user from profile with not validated email
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User selects "NotValidatedEmail" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail email from profile with not validated email"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "NotValidatedEmail"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Gmail email from profile with not validated email"
    Then Validate the message of the email for registered user has value "Gmail email from profile with not validated email"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
#bug-dashboard-704
#    And Validate the profile in the email has value "NotValidatedEmail"
    And Validate the date and time in the email has correct value
    And Validate To value in the email thread has value "Boris Dimitrov <tested23@abv.bg>"
    And Validate From value in the email thread has value "<vereign.automation@gmail.com>"
    And Validate verified icon is not presented on the Email claim of the Email Thread
    And Validate Email field in the email template has value "notValidated@example.com"
    And Validate profile image is presented on the V-card
    And I close the browser

  @inbox @gmail @bug-dashboard-705
  Scenario: Send verified gmail email with registered user in Cc
    Given I open chrome and load chrome extension and chrome profile
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User click on gmail add Cc button
    And User populates gmail To field with random email
    And User populates gmail Cc field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email with registered user in Cc"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email with registered user in Cc"
    Then Validate the message of the email for registered user has value "Verified gmail email with registered user in Cc"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
#bug-dashboard-705
#    And Validate To field in the email thread has the correct value
#    And Validate Cc value in the email thread has value "tested23@abv.bg"
    And I close the browser

  @inbox @gmail @bug-dashboard-705
  Scenario: Send verified gmail email with not registered user in Cc
    Given I open chrome and load chrome extension and chrome profile
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User click on gmail add Cc button
    And User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Cc field with random email
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email with not registered user in Cc"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email with not registered user in Cc"
    Then Validate the message of the email for registered user has value "Verified gmail email with not registered user in Cc"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
#bug-dashboard-705
#    And Validate To value in the email thread has value "Boris Dimitrov"
#    And Validate Cc field in the email thread has the correct value
    And I close the browser

  @inbox @gmail
  Scenario: Validate email certificates from message security
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Validate email certificates from message security"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    When User open the "1" email
    Then Validate the email has the subject used to send the email
    And Click message security and validate all tabs
    And I close the browser

  @inbox @roundcube
  Scenario: Send verified roundcube email to registered user
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Verified roundcube email to registered user"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Verified roundcube email to registered user"
    Then Validate the message of the email for registered user has value "Verified roundcube email to registered user"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "Boris Dimitrov"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the email template has value "Roundcube Robot"
    And Validate Email field in the email template has value "mister.test@kolab-qa.vereign.com"
    And I close the browser

  @inbox @roundcube
  Scenario Outline: Send verified roundcube email to registered user with changed profile
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
    And User selects "<Profile_Name>" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "<Message>"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "<Profile_Name>"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "<Message>"
    Then Validate the message of the email for registered user has value "<Message>"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "<Profile_Name>"
    And Validate the date and time in the email has correct value
    And Validate small footer text "<Expected_Small_Footer_Text>"
    And Validate To value in the email thread has value "Boris Dimitrov"
    And Validate From value in the email thread has value "You"
    And I close the browser
    Examples:
      | Profile_Name | Message                                                            | Expected_Small_Footer_Text                                                                       |
      | No Claims    | Roundcube email to registered user with profile that has no claims | This email has been sent by a verified user and authenticated using the beta version of Vereign. |
      | Name Only    | Roundcube email to registered user with profile that has Name only | This email has been sent by Roundcube Robot and authenticated using the beta version of Vereign. |

  @inbox @roundcube
  Scenario: Send verified roudncube email to registered user from profile with not validated email
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
    And User selects "NotValidatedEmail" from the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube email from profile with not validated email"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "NotValidatedEmail"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Roundcube email from profile with not validated email"
    Then Validate the message of the email for registered user has value "Roundcube email from profile with not validated email"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "NotValidatedEmail"
    And Validate the date and time in the email has correct value
    And Validate To value in the email thread has value "Boris Dimitrov"
    And Validate From value in the email thread has value "You"
    And Validate verified icon is not presented on the Email claim of the Email Thread
    And Validate Email field in the email template has value "notValidated@example.com"
    And I close the browser

  @inbox @roundcube
  Scenario: Send verified roundcube email with registered user in Cc
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field with random email
    And User click on roundcube Add Cc button
    And User populates roundcube Cc field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Verified roundcube email with registered user in Cc"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Verified roundcube email with registered user in Cc"
    Then Validate the message of the email for registered user has value "Verified roundcube email with registered user in Cc"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate From value in the email thread has value "You"
    And Validate To field in the email thread has the correct value
    And Validate Cc value in the email thread has value "tested23@abv.bg"
    And I close the browser

  @inbox @roundcube
  Scenario: Send verified roundcube email with not registered user in Cc
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User click on roundcube Add Cc button
    And User populates roundcube Cc field with random email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Verified roundcube email with not registered user in Cc"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Verified roundcube email with not registered user in Cc"
    Then Validate the message of the email for registered user has value "Verified roundcube email with not registered user in Cc"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate From value in the email thread has value "You"
    And Validate To field in the email thread has the correct value where email is send from roundcube and Cc is not registered
    And Validate Cc field in the email thread has the correct value where email is send from roundcube and Cc is not registered
    And I close the browser

  @inbox @gmail
  Scenario: Send gmail email to multiple not registered receivers
    Given I open chrome and load chrome extension and chrome profile
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with random emails
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send gmail email to multiple not registered receivers"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    And User click on email thread with message "Send gmail email to multiple not registered receivers"
    Then Validate the message of the email for registered user has value "Send gmail email to multiple not registered receivers"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "0"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    Then Validate vereign label in Email thread when email is send to registered user
    And Validate profile image is presented on the V-card
    And Validate To value in the email thread has the correct value
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And I close the browser

  @inbox @roundcube
  Scenario: Send roundcube email to multiple not registered receivers
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field with random emails
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube email to multiple not registered receivers"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    And User click on email thread with message "Roundcube email to multiple not registered receivers"
    Then Validate the message of the email for registered user has value "Roundcube email to multiple not registered receivers"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "0"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    Then Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has the correct value
    And Validate From value in the email thread has value "You"
    And I close the browser

  @inbox @gmail @roundcube
  Scenario: Reply to a gmail email and validate it in the dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Log into dashboard with roundcube email
    And Check if roundcube account is registered and register it if not via VIAM API
#Check if the testing account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And I change the extension environment
    And User Logs in chrome extension with password "1111"
#Send gmail email to the registered roundcube email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail email waiting for reply"
    And User click on gmail Send button
#Forget account credentials
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
#Log into dashboard with the roundcube account
    And User logs into vereign with roundcube account
    And User navigates to roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Reply to the email
    And User click on the received gmail email
    And User click on the roundcube details button in the sent email
    And I wait for {5000} mseconds
    And User click on roundcube Reply button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube Message Field "Repling to gmail email"
    And User click on roundcube Send button
    And User navigates to vereign login page
#    And Validate the subject of the first email is the one used to send email with Reply
#Open the email
    And User open the "1" email
#Validate the email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
#Validate there are 2 items in the email thread
    And Validate there are "2" items in the email thread
#Open the second item from the email thread
    When User click on the "1" item in the email thread
    When User click on the "2" item in the email thread
    Then Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Gmail email waiting for reply"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Vereign Automation"
    And Validate Name field in the "2" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "2" item from the Email Thread has value "vereign.automation@gmail.com"
#Close the second item from the email thread
    When User click on the "2" item in the email thread
#Open the first item from the email thread
    When User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "Vereign Automation"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
#End of the validation with the mister.test@kolab-qa.vereign.com account
    And User logs out of vereign and click on cleanup local identity
#Log into vereign with the gmail account - vereign.automation@gmail.com
    And User logs into vereign with gmail account
#Validate the email
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the profile in the email has value "Email"
    And Validate there are "2" items in the email thread
#Validate the second item in the email thread
    When User click on the "1" item in the email thread
    When User click on the "2" item in the email thread
    Then Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Gmail email waiting for reply"
    And Validate To value in the email thread has value "Roundcube Robot"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "2" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "2" item from the Email Thread has value "vereign.automation@gmail.com"
    When User click on the "2" item in the email thread
#Validate the first item in the email thread
    And User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Roundcube Robot"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
    And I close the browser

  @inbox @gmail @roundcube
  Scenario: Reply to roundcube email and validate it in the dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Validate the gmail account is created
    And Check if gmail account is registered and register it if not via VIAM API
#Validate the roundcube account is created and log in
    And User logs into vereign with roundcube account
#Log into roundcube
    And User navigates to roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Send the email
    And User click on roundcube Compose button
    And I wait for {5000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "vereign.automation@gmail.com"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube email waiting for reply"
    And User click on roundcube Send button
#Logout from the roundcube account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
#Login with the gmail account
    And User logs into vereign with gmail account
#Login into the chrome extension
    And User Logs in chrome extension with password "1111"
#Open the received email
    And User navigates to gmail
    And User click on gmail Inbox folder
    And I wait for {15000} mseconds
    And User click on the received roundcube email
##Click Reply
    And User click on gmail Reply button
    And Validate vereign icon is presented in gmail
    And User populates gmail Message field with value "Replying to roundcube email"
    And User click on gmail Send button
#Navigate to the dashboard and validate the data with the gmail account - vereign.automation@gmail.com
    And User navigates to vereign login page
#Open the first email
    And User open the "1" email
#Validate the email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
#Open the second item from the email thread and validate it
    And Validate there are "2" items in the email thread
    And User click on the "1" item in the email thread
    And User click on the "2" item in the email thread
    And Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Roundcube email waiting for reply"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Roundcube Robot"
    And Validate Name field in the "2" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "2" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Close the second items from the email thread
    And User click on the "2" item in the email thread
#Open the first item in the email thread and validate it
    And User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Replying to roundcube email"
    And Validate To value in the email thread has value "Roundcube Robot"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "1" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "1" item from the Email Thread has value "vereign.automation@gmail.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
#Forget account credentials
    And User logs out of vereign and click on cleanup local identity
#Login into dashboard with roundcube account
    And User logs into vereign with roundcube account
#Open the first email
    And User open the "1" email
#Validate the email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the profile in the email has value "Email"
    And Validate there are "2" items in the email thread
#Open the second item in the email thread and validate it
    When User click on the "1" item in the email thread
    And User click on the "2" item in the email thread
    Then Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Roundcube email waiting for reply"
    And Validate To value in the email thread has value "Vereign Automation"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "2" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "2" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Close the second item from the email thread
    When User click on the "2" item in the email thread
#Open the first item from the email thread and validate it
    And User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Replying to roundcube email"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Vereign Automation"
    And Validate Name field in the "1" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "1" item from the Email Thread has value "vereign.automation@gmail.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
    And I close the browser

  @roundcube @inbox
  Scenario: Send two emails to the same email and validate the header and footer
#Open chrome and navigate to vereign login page
    Given I open Chrome browser and navigate to vereign login page
#Check if the roundcube account is created and log in
    And User logs into vereign with roundcube account
#log into roundcube
    And User navigates to roundcube
    And User logs into roundcube
#Click settings and populate the correct environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
    And User click on Mail button
    And User enables Vereign On
#Compose the first email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field with random email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Check header footer when sending second email. 1st email"
    And User click on roundcube Send button
#Navigate to the dashboard and validate the first email
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate there are "1" items in the email thread
#    And User click on email thread with message "Check header footer when sending second email. 1st email"
    And Validate To value in the email thread has the correct value
    And Validate From value in the email thread has value "You"
    And Validate vereign label in Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Check header footer when sending second email. 1st email"
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To field with the same email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Check header footer when sending second email. 2nd email"
    And User click on roundcube Send button
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate there are "1" items in the email thread
#    And User click on email thread with message "Check header footer when sending second email. 2nd email"
    And Validate To value in the email thread has the correct value
    And Validate From value in the email thread has value "You"
    And Validate vereign label in Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Check header footer when sending second email. 2nd email"
    And I close the browser

  @gmail @search
  Scenario: Search an email by subject and email of the sender/receiver
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to registered user"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
    And User search for the email by the subject
#Validate the search results in the Inbox
    And Validate there are "1" items presented in the Inbox
    And Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the count of the contacts in the inbox is "1"
    And Validate the profile in the inbox has value "Email"
#Validate the search results in the email interaction
    And User open the "1" email
    Then Validate the email has the subject used to send the email
    Then Validate the message of the email for registered user has value "Verified gmail email to registered user"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "Boris Dimitrov <tested23@abv.bg>"
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And Validate Name field in the email template has value "Vereign Automation"
    And Validate Email field in the email template has value "vereign.automation@gmail.com"
    And Validate verified icon is presented on the Email claim of the Email Thread
    And User click on Inbox tab
#Search by the email of the sender and validate the Inbox
    And User populates the Inbox search field with value "vereign.automation@gmail.com"
    And Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the count of the contacts in the inbox is "1"
    And Validate the profile in the inbox has value "Email"
#Search by the email of the receiver and validate the Inbox
    And User click on Contacts tab
    And User click on Inbox tab
    And User populates the Inbox search field with value "tested23@abv.bg"
    And Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the count of the contacts in the inbox is "1"
    And Validate the profile in the inbox has value "Email"
    And I close the browser

  @gmail @search @delete
  Scenario: Delete an email and search for its subject and email of the sender
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to registered user"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
#Delete the email
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
#Search by subject
    And User search for the email by the subject
    And Validate there are "0" items presented in the Inbox
    And Validate no interactions found message is presented
#Search by email
    And User populates the Inbox search field with value "vereign.automation@gmail.com"
    And Validate the subject of the first email is not the one used to send email
    And I close the browser

  @gmail
  Scenario: Send email to yourself
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "vereign.automation@gmail.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to yourself"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "0"
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email to registered user"
    Then Validate the message of the email for registered user has value "Verified gmail email to yourself"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "0"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And Validate Name field in the email template has value "Vereign Automation"
    And Validate Email field in the email template has value "vereign.automation@gmail.com"
    And Validate verified icon is presented on the Email claim of the Email Thread
    And Validate profile image is presented on the V-card
    And I close the browser

  @gmail @wip
  Scenario: Forward an gmail email
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "vereign.automation@gmail.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to yourself"
    And User click on gmail Send button
#Open the first email in the gmail inbox
    And User click on gmail Inbox folder
    And I wait for {5000} mseconds
    And User click on the received roundcube email
#Forward the email
    And User click on gmail Forward button
    And Validate vereign icon is presented in gmail
    And User populates gmail To field with random email
    And User populates gmail Message field with value "Forwarding..."
    And User click on gmail Send button
#Check the dashboard of the sender
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "0"
#Open the email and validate it
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email to registered user"
    And Validate there are "1" items in the email thread
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "0"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate From value in the email thread has value "You"
    And Validate To value in the email thread has the correct value
    And Validate Name field in the email template has value "Vereign Automation"
    And Validate Email field in the email template has value "vereign.automation@gmail.com"
    And Validate verified icon is presented on the Email claim of the Email Thread
    And I wait for {60000} mseconds


























