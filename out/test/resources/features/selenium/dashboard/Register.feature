#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Register a new user

  Background:
    Given we are testing the VIAM Api

  @register
  Scenario: Register user with email and valid data.
    Given I open Chrome browser and navigate to vereign login page
#Sign in to Vereign Page
    When User clicks on Create Account button
#Welcome to your digital self page
    And  User populates the Email address or mobile number field with valid email
    And User click on Confirm button
#Check your email page
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Almost there Page
    When User populates first name "Boris"
    And User populates last name "Dimitrov"
    And User populates phone "+359884045187"
    When User Agrees to the beta particiation agreement
    And User click on Confirm button
#Create your PIN page
    When User populates the New Pin Code and Confirm New Pin Code field with "1111" and "1111"
    When User click on Confirm button
#Logged in
    Then User is redirected to Vereign Inbox
    And Title of the page is "Vereign | My Inbox"
    And message is presented "Vereign is currently in Beta"
    When User click on Continue button
    And I close the browser

  @register
  Scenario: Send another code - Check you email
    Given I open Chrome browser and navigate to vereign login page
#Sign in to Vereign Page
    When User clicks on Create Account button
#Welcome to your digital self page
    And  User populates the Email address or mobile number field with valid email
    And User click on Confirm button
    And User click on send another code button
    And Error Message is presented with text "Please wait 60 seconds between tries." and message disappears
    And I wait for {60000} mseconds
    And User click on send another code button
    And Confirmation Message is presented with text "A new code has been sent" and message disappears
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    And Error message is not presented
    And Validate let's get you started page labels
    And I close the browser


  @negative @register
  Scenario: Try to register an user with invalid data
    Given I open Chrome browser and navigate to vereign login page
#Sign in to vereign page
    When User clicks on Create Account button
#Welcome to your digital self page
    And User populates the Email address or mobile number field with invalid email
    Then Confirm button is disabled
    When  User populates the Email address or mobile number field with valid email
    Then Confirm button is enabled
    When User click on Confirm button
#Check your email page
    And User populates the confirmation code "1234" "5678"
    And User click on Confirm button
    Then Error Message is presented with text "Incorrect confirmation code provided." and message disappears
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Almost there page
    And User Agrees to the beta particiation agreement
    When User click on Confirm button
    Then Error Message "Please complete the field above" is presented under first name, last name and mobile fields
    When User populates first name "Boris"
    And User populates last name "Dimitrov"
    And User populates phone "+359884045187"
    Then error messages under first name,last name and mobile fields are not presented
    When User click on Confirm button
#Create your PIN page
    And I wait for {5000} mseconds
    Then Confirm button is disabled
    When User populates the New Pin Code and Confirm New Pin Code field with "1111" and "1112"
    Then Confirm button is disabled
    When User populates the New Pin Code and Confirm New Pin Code field with "1111" and "1111"
    Then Confirm button is enabled
    And I close the browser

  @negative @register
  Scenario: Wrong Confirmation Code Upon Registration with 5 invalid inputs and a valid one after that
    Given I open Chrome browser and navigate to vereign login page
#Sign in to vereign page
    When User clicks on Create Account button
#Welcome to your digital self page
    And User populates the Email address or mobile number field with valid email
    And User click on Confirm button
#Check your email page
    When User populates the confirmation code "1111" "1111"
    And User click on Confirm button
    Then Error Message is presented with text "Incorrect confirmation code provided." and message disappears
    When User populates the confirmation code "2222" "2222"
    And User click on Confirm button
    Then Error Message is presented with text "Incorrect confirmation code provided." and message disappears
    When User populates the confirmation code "3333" "3333"
    And User click on Confirm button
    Then Error Message is presented with text "Incorrect confirmation code provided." and message disappears
    When User populates the confirmation code "4444" "4444"
    And User click on Confirm button
    Then Error Message is presented with text "Please wait a little bit between each try." and message disappears
    When User populates the confirmation code "5555" "5555"
    And User click on Confirm button
    Then Error Message is presented with text that contains "This Email is blocked for 23h59m" and error message disappears
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    Then Error Message is presented with text that contains "This Email is blocked for 23h59m" and error message disappears
    And I close the browser

  @register
  Scenario: Try to register a user without checking the beta participation agreement checkbox
    Given I open Chrome browser and navigate to vereign login page
    When User clicks on Create Account button
#Welcome to your digital self page
    And  User populates the Email address or mobile number field with valid email
    And User click on Confirm button
#Check your email page
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Almost there page
    When User populates first name "Boris"
    And User populates last name "Dimitrov"
    And User populates phone "+359884045187"
    And Confirm button is disabled
    And I close the browser

  @register
  Scenario: Register a user with valid email and use the same email for another registration
    Given I open Chrome browser and navigate to vereign login page
    When User clicks on Create Account button
#Welcome to your digital self page
    And User populates the Email address or mobile number field with valid email
    And User click on Confirm button
#Check your email page
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Almost there page
    And User populates first name "Boris"
    And User populates last name "Dimitrov"
    And User populates phone "+359884045187"
    And User Agrees to the beta particiation agreement
    And User click on Confirm button
#Create your PIN page
    And User populates the New Pin Code and Confirm New Pin Code field with "1111" and "1111"
    And User click on Confirm button
    And User click on Continue button
#Logged in
    And User logs out of vereign and click on cleanup local identity
#Logged out
    When User clicks on Create Account button
#Welcome to your digital self page
    And User populates the Email/Mobile field with the same email address in Create Account
    And User click on Confirm button
    Then Error Message is presented with text "Identificator is already taken" and message disappears
    And I close the browser

  @register
  Scenario: Validate confirmation code info message when registering user with valid phone
    Given I open Chrome browser and navigate to vereign login page
    When User clicks on Create Account button
#Welcome to your digital self page
    And User populates the Email address or mobile number field with "+359884045187"
    And User click on Confirm button
    And I wait for {2000} mseconds
    Then Main Info Message has the correct text "Check your phone"
    And Validate page contains element with text "We've sent a 8-digit confirmation code to your phone"
    And Validate page contains element with text "It will expire shortly, so enter your code soon."
    And Validate page contains element with text "Keep this window open while checking for your code."
    And Validate page contains element with text "Haven't received anything? "
    And Validate page contains element with text "Send another code."
    And I close the browser


  @register
  Scenario Outline: Validating all labels in registration flow
    Given I open Chrome browser and navigate to vereign login page
#Sign in to Vereign Page
    Then Main Info Message has the correct text "Sign in to Vereign"
    And Validate Link is presented with text "www.vereign.com" and href "https://www.vereign.com"
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User clicks on Create Account button
#Welcome to your digital self page
    Then Main Info Message has the correct text "Welcome to your digital self"
    And Validate Welcome to your digital self page labels
    And Validate Link is presented with text "Find out more" and href "https://vereign.com"
    And Placeholder of the field has value "Email or mobile number"
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    And Confirm button is disabled
    When User populates the Email address or mobile number field with valid email
    And User click on Confirm button
#Check your email page
    Then Main Info Message has the correct text "Check your email"
    And Validate page contains element with text "We've sent a 8-digit confirmation code to your email"
    And Validate page contains element with text "It will expire shortly, so enter your code soon."
    And Validate page contains element with text "Your confirmation code"
    And Placeholder of the field has value "XXXX"
    And Validate page contains element with text "Keep this window open while checking for your code."
    And Validate Check you email or phone page labels
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Almost there page
    Then Main Info Message has the correct text "<lets_get_you_started>"
    And Placeholder of the field has value "First name"
    And Placeholder of the field has value "Last name"
    And Placeholder of the field has value "Mobile"
    And Validate let's get you started page labels
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User populates first name "Boris"
    And User populates last name "Dimitrov"
    And User populates phone "12"
    Then Phone number popup is presented with the correct text
    And User populates phone "+359884045187"
    When User Agrees to the beta particiation agreement
    And User click on Confirm button
#Create your PIN page
    Then Main Info Message has the correct text "Set your pin"
    And Validate page contains element with text "<pin_code_message>"
    And Placeholder of the field has value "XXXX"
    And Validate page contains element with text "Pin Code"
    And Validate page contains element with text "Confirm Pin Code"
    And Confirm button is disabled
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    And I close the browser
    Examples:
      | lets_get_you_started  | pin_code_message                                                    |
      | Let's get you started | You will use this pin later to unlock your identity on this device. |

  @register
  Scenario: Try to register with a phone that is validated by another user
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Add the phone claim
    And User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Phone" option from secondary Identity select
    And User populates the field with placeholder "Type your phone number here" with random phone
    And User click on Add to your Identity button
    And User expand all folders
#Validate the email
    When User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    Then Validate phone field is validated in the Identity Page
    And User logs out of vereign and click on cleanup local identity
    And User clicks on Create Account button
    And User populates the email/mobile field with the same phone number
    And User click on Confirm button
    And Error Message is presented with text "Identificator is already taken" and message disappears
    And I close the browser

  @register @bug-dashboard-630
  Scenario: Delete a validated claim and use it for registration
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Add the phone claim
    And User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Phone" option from secondary Identity select
    And User populates the field with placeholder "Type your phone number here" with random phone
    And User click on Add to your Identity button
    And User expand all folders
#Validate the email
    When User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Delete the claim
    And User click on delete "Phone" claim button
    And User confirms the deletion
#Logout
    And User logs out of vereign and click on cleanup local identity
    And User clicks on Create Account button
    And User populates the email/mobile field with the same phone number
    And User click on Confirm button
#bug-dashboard-630
#    And Error message is not presented
    And I close the browser






