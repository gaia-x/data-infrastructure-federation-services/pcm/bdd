#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @parallel
Feature: Chrome extension

  Background:
  Given we are testing the VIAM Api

  @gmail @bug-dashboard-542
  Scenario: Login with invalid PIN and validate user is blocked in dashboard
    Given I open chrome and load chrome extension
    When I change the extension environment
#Chrome extension and profile loaded
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Dashboard Logged in
    And User navigates to the extension URL
#first attempt
    And User populates chrome extension PIN "2222"
    And User click on chrome extension Send button
    Then Error Message is presented in the chrome extension page with text "Wrong pincode"
#Second Attempt
    When User click on chrome extension Send button
    Then Error Message is presented in the chrome extension page with text "Wrong pincode"
#Third Attempt
    When User click on chrome extension Send button
    Then Error Message is presented in the chrome extension page with text "3 failed attempts. Identity is locked!"
#Fourth Attempt
    When User click on chrome extension Send button
    Then Error Message is presented in the chrome extension page with text that contains "Your identity has been locked. Try again in "
#Validate that the error message will be shown even with valid PIN
    When User populates chrome extension PIN "1111"
    And User click on chrome extension Send button
    Then Error Message is presented in the chrome extension page with text that contains "Your identity has been locked. Try again in "
    And I wait for {2000} mseconds
#    When User navigates to vereign login page
#    And User populates PIN code field "1111"
#    And User click on Authenticate button
#    Then Error Message is presented with text that contains "Your identity has been locked. Try again in 4 minutes and " and error message disappears
    And I close the browser

  @gmail
  Scenario: Logout and login into chrome extension with valid PIN
    Given I open chrome and load chrome extension
    When I change the extension environment
#Chrome extension and profile loaded
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Dashboard Logged in
    And User navigates to the extension URL
    And User navigates to the extension URL in new window
    And User populates chrome extension PIN "1111"
    And User click on chrome extension Send button
    And I wait for {2000} mseconds
    And I refresh the current page
    And User navigates to the extension URL
    And Validate Logged in as label is presented
    And Validate the presented credentials are "BD"
    And Validate chrome extension Privacy Policy link has the correct href and text
    When User click on chrome extension Logout button
    And Validate Logged in as label is not presented
    And Validate Choose Identity labels is presented
    And Validate the presented credentials are "BD"
    And I close the browser

  @gmail
  Scenario: Login with account that have image claim
    Given I open chrome and load chrome extension
    When I change the extension environment
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
    And User click on Identity tab
    And User upload a photo with valid format to the identity
    Then Validate image is presented
    When User click on Profiles tab
    And User click on profile with name "Email"
    And I wait for {500} mseconds
    And User Adds the image claim in the profile
    And Validate image is presented in the current profile
    And User click on Inbox tab
    And User navigates to the extension URL
    And User navigates to the extension URL in new window
    And User populates chrome extension PIN "1111"
    And User click on chrome extension Send button
    And I wait for {2000} mseconds
    And I refresh the current page
    And Validate chrome extension image is presented
    And I close the browser

  @gmail
  Scenario: Logout from extension and validate user is logged out from dashboard
#Open chrome and load the chrome extension
    Given I open chrome and load chrome extension
    When I change the extension environment
#Register the new user
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Dashboard Logged in
    And User click on close tour button
#Login into chrome extension in new window
    And User navigates to the extension URL in new window
    And User populates chrome extension PIN "1111"
    And User click on chrome extension Send button
#Logout from chrome extension
    And User navigates to the extension URL in new window
    And I wait for {2000} mseconds
    And I refresh the current page
    And User click on chrome extension Logout button
    And Validate Logged in as label is not presented
#Validate user is logged out of the dashboard
    And Validate that user is logged out of the dashboard after logging out of chrome extension
    And I close the browser


