#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @notParallel
Feature: Email clients

  Background:
    Given we are testing the VIAM Api

  @roundcube
  Scenario: Send email from roundcube and validate it in roundcube
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Log in with testing account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Validate all profiles are presented
    And User click on roundcube profile select
    And User Validates roudncube profiles count is "7"
    And User Validates roundcube profile with name "Social" is presented
    And User Validates roundcube profile with name "Email" is presented
    And User Validates roundcube profile with name "Friends" is presented
    And User Validates roundcube profile with name "NotValidatedEmail" is presented
    And User Validates roundcube profile with name "No Claims" is presented
    And User Validates roundcube profile with name "Name Only" is presented
    And User Validates roundcube profile with name "Contacts" is presented
    And User close the roundcube profile select
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User click on roundcube Add Cc button
    And User populates roundcube Cc field "randomRec3456@example.com"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Roundcube client validation"
    And User click on roundcube Send button
#Validate the email in the sent folder
    When User click on roundcube Sent folder
    And User click on the email in the roundcube sent folder
    And User click on the roundcube details button in the sent email
    And User Validates roundcube sent email From field has value "Mister Test"
    And User Validates roundcube sent email To field has value "tested23@abv.bg"
    And User Validates roundcube sent email Cc field has value "randomRec3456@example.com"
    And User Validates roundcube sent email Message field has value "Roundcube client validation"
    And I close the browser

  @gmail
  Scenario: Send email from gmail and validate it in gmail
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
    When I navigate to 10minuteEmail and save the email address
#Change the environment
    When I change the extension environment
    And Check if testing account is registered and register it if not via VIAM API
#Log into vereign with testing account - vereign.automation@gmail.com
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
    And User navigates to gmail
#Validate all profiles are presented
    And User Validates gmail profiles count is "7"
    And User Validates gmail profile with name "Social" is presented
    And User Validates gmail profile with name "Email" is presented
    And User Validates gmail profile with name "Friends" is presented
    And User Validates gmail profile with name "NotValidatedEmail" is presented
    And User Validates gmail profile with name "No Claims" is presented
    And User Validates gmail profile with name "Name Only" is presented
    And User Validates gmail profile with name "Contacts" is presented
#Send the email
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with the email from 10minuteEmail
    And User click on gmail add Cc button
    And User populates gmail Cc field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail Client validation"
    And User click on gmail Send button
#Validate the email in the sent folder
    And User click on gmail Sent folder
    And User click on the email in the gmail sent folder
    And User Validates gmail sent email From field has value "vereign.automation@gmail.com"
    And User validates gmail sent email To field has the correct value
    And User Validates gmail sent email Cc field has value "tested23@abv.bg"
    And User Validates gmail sent email Message field has value "Gmail Client validation"
    And I close the browser



