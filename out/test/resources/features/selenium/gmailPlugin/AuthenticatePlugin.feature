#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#Author: Rosen Georgiev rosen.georgiev@vereign.com

@gmail @all
Feature: GMAIL plugin - Authenticate
  Authenticate the Vereign Gmail Plugin

  Background:
    #Given we are testing the JS lib

  @authGmailPlugin @wip
  Scenario: Authenticate a Gmail account to the Vereign Gmail Extension - Positive
    Given I start the browser {chrome}
    Then I authenticate the Vereign Gmail extension with the google account
    #Then I close the browser
