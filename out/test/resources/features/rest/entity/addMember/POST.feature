#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addMember
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @member @all
Feature: VIAM - entity - addMember POST
  Add a new member link between entities

  Background:
    Given we are testing the VIAM Api

  @addMember
  Scenario: Add member link to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I create member link
    Given I clear the request body
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {member} from DataContainer into currentRequest Body with key {memberUUID}
    Then I create new member link via VIAP API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addMember @negative @bug-rest-45 @wip
  Scenario: Try to add member link to an entity a second time - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I create member link
    Given I clear the request body
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {member} from DataContainer into currentRequest Body with key {memberUUID}
    Then I create new member link via VIAP API
    And the field {status} has the value {OK}
#Try to add the same link a second time
    Then I create new member link via VIAP API
    And the field {status} has the value {Some error}

  @addMember @negative
  Scenario Outline: Try to add a member link to an entity with invalid params [<profile>] - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I try to create member link
    Given I clear the request body
    And I load the REST request {Member.json} with profile {<profile>}
    Then I create new member link via VIAP API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile                 | status                                   | data                                                             |
      | missing_member          | There was an error with the input fields | There was an error with the input fields: memberUUID is required |
      | empty_member            | There was an error with the input fields | There was an error with the input fields: memberUUID is required |
     # | invalid_uuid_member     | Error adding member                      | Error adding opposite relation:Error adding a relation: could not find entity. |
      | missing_entityuuid      | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
      | empty_entityuuid        | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
      | invalid_uuid_entityuuid | Error adding member                      | Error adding a relation: could not find entity                   |
      | non_existing_uuid       | Error adding member                      | Error adding a relation: could not find entity                   |

