#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/deleteClaim
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @all
Feature: VIAM - entity - deleteClaim POST
  Delete a claim to an entity

  Background:
    Given we are testing the VIAM Api

  @deleteClaim
  Scenario: Delete an existing claim to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Delete an existing claim
    Given I clear the request body
    Then I delete a claim {age} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {OK}

  @deleteClaim @negative
  Scenario Outline: Try to Delete a claims that were required for the registration - [<claim>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    #Get the data of current entity
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
#Delete an existing claim
    Given I clear the request body
    Then I delete a claim {<claim>} with tag {<tag>} of the current member via VIAM API
    And the field {status} has the value {Cannot delete default claim}
    And the field {code} has the value {400}
    Examples:
      | claim        | tag          |
      | name         | personalName |
      | emails       | registration |
      | phoneNumbers | registration |

  @deleteClaim @negative
  Scenario: Try to Delete a non existing claim to an entity - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Delete an existing claim
    Given I clear the request body
    Then I delete a claim {nonExistingClaim} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {Cannot delete claim. Missing claim: nonExistingClaim}
    And the field {code} has the value {400}

  @deleteClaim @negative
  Scenario Outline: Try to Delete a claim with invalid params [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Delete an existing claim
    Given I clear the request body
    Given I load the REST request {DeleteClaims.json} with profile {<profile>}
    Then I delete a claim of an entity via VIAM API
    And the field {status} has the value {<error>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile            | error                                    | data                                                             |
      | missing_claim      | There was an error with the input fields | There was an error with the input fields: claim is required      |
      | empty_claim        | There was an error with the input fields | There was an error with the input fields: claim is required      |
      | missing_entityuuid | There was an error with the input fields | There was an error with the input fields: entityuuid is required |
      | empty_entityuuid   | There was an error with the input fields | There was an error with the input fields: entityuuid is required |
      | empty_tag          | There was an error with the input fields | There was an error with the input fields: tag is required        |
      | missing_tag        | There was an error with the input fields | There was an error with the input fields: tag is required        |

  @deleteClaim @negative
  Scenario: Try to  Delete an already deleted claim - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Delete the new claim
    Given I clear the request body
    Then I delete a claim {age} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {OK}
#Try to Delete the same claim a second time
    Given I clear the request body
    Then I delete a claim {age} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {Cannot delete claim. Missing claim: age}
    And the field {code} has the value {400}

  @deleteClaim @negative
  Scenario Outline: Try to Delete a claim without proper authorization - [header] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Delete an existing claim
    Given I clear the request body
    And I delete the headers
      | <header> |
    Then I delete a claim {name} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | token     |
      | uuid      |
      | publicKey |

  @deleteClaim @negative @bug-rest-6 @wip
  Scenario: Try to Delete an existing claim to an another entity - Neggative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Session} from the last response and store it in the DataContainer with key {UserToken_1}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {UserUUID_1}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.claim} from the last Request Body and store it in the DataContainer with key {claim_1}
#Create a second member
    Given I clear the request body
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the second member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Try to Delete a claim of the first user while logged as the second
    Given I clear the request body
    Then I load object with key {claim_1} from DataContainer into currentRequest Body with key {claim}
    Then I load object with key {UserUUID_1} from DataContainer into currentRequest Body with key {entityuuid}
    And I set the request fields
      | tag | ageValue |
    Then I delete a claim of an entity via VIAM API
    And the field {status} has the value {Some error}