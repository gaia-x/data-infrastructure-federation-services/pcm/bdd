#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/createEntity
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @guardian @all
Feature: VIAM - entity - createEntity POST
  Create a new entity

  Background:
    Given we are testing the VIAM Api

  @createEntity
  Scenario Outline: Create a new entity with Type [<type>] - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity
    Given I clear the request body
    Then I create a new entity with type {<type>} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} is present and not empty
    Examples:
      | type |
      | 1    |
      | 2    |
      | 3    |
      | 4    |
      | 5    |
      | 6    |

  @createEntity @negative @bug-rest-30
  Scenario Outline: Try to create a new entity with invalid data [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity
    Given I clear the request body
    Then I load the REST request {Entity.json} with profile {<profile>}
    Then I create a new entity via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile      | data                                                               |
    #bug-rest-30  | empty_type   | There was an error with the input fields: type is required         |
      | missing_type | There was an error with the input fields: type is required         |
      | type_7       | There was an error with the input fields: type has incorrect value |