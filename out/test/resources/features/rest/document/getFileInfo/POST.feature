#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/document/getFileInfo
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @document @all
Feature: VIAM - document - getFileInfo POST
  This endpoint is for getting the file info.

  Background:
    Given we are testing the VIAM Api

  @getFileInfo
  Scenario: Create a new document and then Get the file info - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUUID}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#Get the file info of the new document
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.GetContentType} has the value {application/pdf}
    And the field {$.data.GetContentLength} has the value {0}
    And the field {$.data.BoxType} has the value {A}
    And the field {$.data.DisplayName} has the value {123456}
    And the field {$.data.Author} has the value stored in DataContainer with key {entityUUID}

  @getFileInfo @negative
  Scenario Outline: Try to get the file info without required headers [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#Get the file info of the new document
    And I delete the headers
      | <header> |
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    Examples:
      | header       | status                    | code |
      | token        | No authentication values  | 400  |
      | uuid         | No authentication values  | 400  |
      | passportuuid | No passport UUID provided | 401  |

  @getFileInfo
  Scenario: Update a document and then Get the file info - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUUID}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | createdpdf.pdf  |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#Get the document
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {OK}
#Then I get the file
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.GetContentType} has the value {application/pdf}
    And the field {$.data.GetContentLength} has the value {7125}
    And the field {$.data.BoxType} has the value {A}
   # And the field {$.data.DisplayName} has the value {testpdf.pdf}
    And the field {$.data.Author} has the value stored in DataContainer with key {entityUUID}

  @getFileInfo @bug-rest-51 @wip
  Scenario: Update 2 file versions and then get the file info - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | createdPdf.pdf  |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {OK}
#Then I get the file
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.GetContentType} has the value {application/pdf}
#Try to put it a second time
    And I set the headers
      | contenttype | text/plain |
    Then I update the current document with pdf {test.txt} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Then I get the file
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.GetContentType} has the value {text/plain}

  @getFileInfo
  Scenario: Update a file version with txt file and get the file info - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | text/plain     |
      | path        | createdtxt.txt |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {test.txt} via VIAM API
    And the field {status} has the value {OK}
#Get the file info
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.GetContentType} has the value {text/plain}
    And the field {$.data.GetContentLength} has the value {13}