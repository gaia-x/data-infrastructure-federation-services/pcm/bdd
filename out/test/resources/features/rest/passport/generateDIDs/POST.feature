#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/passport/generateDIDs
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @hyperledger @passport @all @wip
Feature: VIAM - passport - generateDIDs POST
  This endpoint is for generating DIDs record and storing it in the HyperLedger-agent.

  Background:
    Given we are testing the VIAM Api

  @generateDID
  Scenario: Generate DID with a passport - Positive
    Given I set the headers
      | publicKey | lQ6eTBloJINOxKoOfFjvtO0qP4NFsD8sOHIgI+/qhafaY2HmzRAufu2iUSh0OcuxDVwqbkc1ztwCl1CR2JPE6owmtaR9+6Ku+D1Jh7VvbljlkiX9+0A5CXdTlhV7bwK7yD12D+P0EYflwQqxplcotmWyr1HQxW+VQVr/VjXA2Q3AbCsS27m3n3nSE7YH2tw+QN6Ix71ozI2RXQ9rQHIBHpwCLDAz/n+VuI2MaYfAlD5lc/qXUxDVzzyl8idU3b39X3bBcg0Q0v+qt+gGYYBc3pJjSmqV6EfU6uFLfj80/G7qGSCOKy8aSnRbpMEr4NZre5KN92SG3bBCBETMFZlEFQ== |
    Then I login member with mode previousaddeddevice via VIAM API
#Then I generate the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I generate a DID for a passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the value of field {data} should contain the text {-----BEGIN CERTIFICATE-----}

  @generateDID @negative
  Scenario: Try to Generate DID with a passport without authentication - Negative
#Then I generate the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I generate a DID for a passport via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}

  @generateDID @negative @bug-rest-106 @wip
  Scenario: Try to Generate DID with a passport without user that is not viewer/owner of the passport - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I generate the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I generate a DID for a passport via VIAM API
    And the field {status} has the value {??}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}

  @generateDID @negative
  Scenario Outline: Try to Generate DID with invalid passport [<value>] - Negative
    Given I set the headers
      | publicKey | lQ6eTBloJINOxKoOfFjvtO0qP4NFsD8sOHIgI+/qhafaY2HmzRAufu2iUSh0OcuxDVwqbkc1ztwCl1CR2JPE6owmtaR9+6Ku+D1Jh7VvbljlkiX9+0A5CXdTlhV7bwK7yD12D+P0EYflwQqxplcotmWyr1HQxW+VQVr/VjXA2Q3AbCsS27m3n3nSE7YH2tw+QN6Ix71ozI2RXQ9rQHIBHpwCLDAz/n+VuI2MaYfAlD5lc/qXUxDVzzyl8idU3b39X3bBcg0Q0v+qt+gGYYBc3pJjSmqV6EfU6uFLfj80/G7qGSCOKy8aSnRbpMEr4NZre5KN92SG3bBCBETMFZlEFQ== |
    Then I login member with mode previousaddeddevice via VIAM API
#Then I generate the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | <value> |
    Then I generate a DID for a passport via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
  Examples:
      | value                                | status                                                                                            | data                                                               |
      |                                      | There was an error with the input fields                                                          | There was an error with the input fields: passportUUID is required |
      | d37388e2-73ce-4d59-8512-fda5f0168f35 | can't generate DID:Error retrieving entity d37388e2-73ce-4d59-8512-fda5f0168f35: response is null | {}                                                                 |
      | d37388e2-73ce-4d59-8512              | can't generate DID:Error retrieving entity d37388e2-73ce-4d59-8512: response is null              | {}                                                                 |
