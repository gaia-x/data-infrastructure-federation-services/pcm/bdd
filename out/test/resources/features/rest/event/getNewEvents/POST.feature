#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/event/getNewEvents
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/eventGetNewEvents.md

@rest @event @all
Feature: VIAM - event - getNewEvents POST
  Calls historically events for entity or for given device per its public key

  Background:
    Given we are testing the VIAM Api

  @getNewEvents
  Scenario: Get all the new events of the current entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Get the events
    And I clear the request body
    Given I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].type} has the value {ActionConfirmedAndExecuted}

  @getNewEvents
  Scenario: Call getNewEvents to get a cancled event of the current entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.ActionID} is present and not empty
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Then I cancel the action
    Then I cancel the current action via VIAM API
    And the field {status} has the value {OK}
#Get the events
    And I clear the request body
    Given I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].type} has the value {Cancelled}

  @getNewEvents
  Scenario: Get all the new events of the current publicKey - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Get the events
    And I clear the request body
    Given I get the new events with mode devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @updateLastViewed @getNewEvents
  Scenario: Update the last viewed timestamp of the current entity and then Get it - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Get the events
    Given I clear the request body
    Then I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].type} has the value {ActionConfirmedAndExecuted}
    And I get the value of {$.data[0].stamp} from the last response and store it in the DataContainer with key {eventStamp}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId2}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#I update the event viewed timestamp
    Given I clear the request body
    And I load object with key {eventStamp} from DataContainer into currentRequest Body with key {lastViewed}
    Then I update the last viewed event with mode entity via VIAM API
    And the field {status} has the value {OK}
#Get the events
    Given I clear the request body
    Then I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId2}
    And the field {$.data[0].type} has the value {ActionConfirmedAndExecuted}

