#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/confirmNewCarddavPublicKey
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all @wip @bug-rest-260
Feature: VIAM - identity - register2FAPublicKey POST
  Register 2FA publicKey with username nad password

  Background:
    Given we are testing the VIAM Api

  @confirmNewCarddavPublicKey
  Scenario: Confirm New CardDav publicKey - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the last request and store it in DataContainer using key {publicKey1}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Register the 2FA publicKey
    And I add a new publicKey header to the currentRequest
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.actionID} from the last response and store it in the DataContainer with key {actionId}
#Get Action and QRcode
    Given I clear the request body
    Given I get the actions without session for a devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data[0].qrCode} from the last response and store it in the DataContainer with key {qrCode}
#Then I confirm the new cardDav publicKey
    Given I clear the request body
    And I load object with key {publicKey1} from DataContainer into currentRequest HEADER {publicKey}
    Then I load object with key {qrCode} from DataContainer into currentRequest Body with key {code}
    Then I load object with key {actionId} from DataContainer into currentRequest Body with key {actionID}
    Then I confirm CardDav publicKey via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}

  @confirmNewCarddavPublicKey @negative
  Scenario Outline: Try to Confirm New CardDav publicKey with invalid qrCode [<code>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the last request and store it in DataContainer using key {publicKey1}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Register the 2FA publicKey
    And I add a new publicKey header to the currentRequest
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.actionID} from the last response and store it in the DataContainer with key {actionId}
#Get Action and QRcode
    Given I clear the request body
    Given I get the actions without session for a devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data[0].qrCode} from the last response and store it in the DataContainer with key {qrCode}
#Then I confirm the new cardDav publicKey
    Given I clear the request body
    Then I load object with key {qrCode} from DataContainer into currentRequest Body with key {code}
    And I load object with key {publicKey1} from DataContainer into currentRequest HEADER {publicKey}
    Then I load object with key {actionId} from DataContainer into currentRequest Body with key {actionID}
    Then I set the request fields
      | <key> | <value> |
    Then I confirm CardDav publicKey via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | key      | value | status                                   | data                                                           |
      | code     |       | There was an error with the input fields | There was an error with the input fields: Code is required     |
      | code     | dsada | Action not succesful                     | Wrong code                                                     |
      | actionID |       | There was an error with the input fields | There was an error with the input fields: ActionID is required |
      | actionID | dsada | Action not succesful                     | No such actionID                                               |

  @confirmNewCarddavPublicKey @negative
  Scenario Outline: Try to Confirm New CardDav publicKey with missing auth header [<header>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the last request and store it in DataContainer using key {publicKey1}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Register the 2FA publicKey
    And I add a new publicKey header to the currentRequest
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.actionID} from the last response and store it in the DataContainer with key {actionId}
#Get Action and QRcode
    Given I clear the request body
    Given I get the actions without session for a devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data[0].qrCode} from the last response and store it in the DataContainer with key {qrCode}
#Then I confirm the new cardDav publicKey
    Given I clear the request body
    Then I load object with key {qrCode} from DataContainer into currentRequest Body with key {code}
    And I load object with key {publicKey1} from DataContainer into currentRequest HEADER {publicKey}
    Then I load object with key {actionId} from DataContainer into currentRequest Body with key {actionID}
    Then I delete the headers
      | <header> |
    Then I confirm CardDav publicKey via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}
    Examples:
      | header    |
      | token     |
      | uuid      |
      | publicKey |