#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/authorizeDevice
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @deviceManager @identity @all
Feature: VIAM - identity - listDevices POST
  This endpoints is for listing all the devices of an user

  Background:
    Given we are testing the VIAM Api

  @listDevices
  Scenario: List the devices for a user - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Then I list the devices
    Given I clear the request body
    Then I set the following request body {{}}
    Given I list the devices via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}

  @listDevices
  Scenario: List devices after device is renamed
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Rename the Device
    And I rename the current device to {Device Renamed} via VIAM API
    And the field {status} has the value {OK}
#List the devices
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {$..DeviceName} has the value {["Device Renamed"]}

  @listDevices @negative
  Scenario: Try to list devices with a revoked device
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Revoke the Device
    And I revoke the current device via VIAM API
    And the field {status} has the value {OK}
#List the devices
    And I clear the request body
    And I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {Device revoked}
    And the field {code} has the value {401}
    And the field {data} has the value {{}}

  @listDevices
  Scenario: List devices after device is revoked when there are two devices
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {$..data[1].Status} has the value {[3]}
#Revoke the Device
    And I revoke the current device via VIAM API
    And the field {status} has the value {OK}
#List the devices
    And I clear the request body
    And I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$..data[1].Status} has the value {[2]}

  @listDevices @negative
  Scenario: Try to List devices with a suspended device
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List Devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Suspend the Device
    And I suspend the current device via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {Device suspended}
    And the field {code} has the value {401}
    And the field {data} has the value {{}}

  @listDevices
  Scenario: List devices after device is suspended when there are two devices
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with previousaddeddevice
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {$..data[0].Status} has the value {[0]}
    And the field {$..data[1].Status} has the value {[3]}
#authorize the new device
    And I authorize the current new device via VIAM API
    Then the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {$..data[0].Status} has the value {[0]}
    And the field {$..data[1].Status} has the value {[0]}
#Suspend the Device
    And I suspend the current device via VIAM API
    And the field {status} has the value {OK}
#List the devices
    And I clear the request body
    And I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$..data[0].Status} has the value {[0]}
    And the field {$..data[1].Status} has the value {[1]}






