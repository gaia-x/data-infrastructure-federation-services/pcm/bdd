#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/addNewDevice
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - addNewDevice POST
  Add a new device to an authenticated user

  Background:
    Given we are testing the VIAM Api

  @addNewDevice
  Scenario: Add a new device to an authententicated user - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.QrCode} is present and not empty
    And the field {$.data.ActionID} is present and not empty

  @addNewDevice @negative
  Scenario: Try to add a new device with unauthenticated device - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}

  @addNewDevice @negative
  Scenario Outline: Try to Add a new device without auth header [<header>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Then I delete the headers
      | <header> |
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |
