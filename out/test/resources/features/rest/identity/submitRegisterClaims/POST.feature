#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://env/api/identity/submitRegisterClaims
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - submitRegisterClaims POST
  This call adds register claims to an identity

  Background:
    Given we are testing the VIAM Api

  @submitRegisterClaims
  Scenario: Submit RegisterClaims with identificator Email - Positive
  #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @submitRegisterClaims
  Scenario: Submit RegisterClaims with identificator phone - Positive
  #Add identificator
    Given I submit a new identificator with random phoneNumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator phonenumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @submitRegisterClaims @negative
  Scenario Outline: Try to submit register claims with missing required param [<param>]- Negative
 #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Try to Add register claims
    Given I load the REST request {Register.json} with profile {create}
    And I remove the following Keys from current request Body:
      | <param> |
    Then I submit registration claims via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | param       | data                                                              |
      | givenname   | There was an error with the input fields: givenname is required   |
      | phonenumber | There was an error with the input fields: phonenumber is required |
      | familyname  | There was an error with the input fields: familyname is required  |
      | email       | There was an error with the input fields: email is required       |

  @submitRegisterClaims @negative
  Scenario Outline: Try to submit registration claims with invalid data [<profile>] - Negative
  #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Try to submit register claims
    Given I load the REST request {Register.json} with profile {<profile>}
    Then I submit registration claims via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile          | data                                                              |
      | empty_email      | There was an error with the input fields: email is required       |
      | empty_name       | There was an error with the input fields: givenname is required   |
      | empty_phone      | There was an error with the input fields: phonenumber is required |
      | empty_familyname | There was an error with the input fields: familyname is required  |

  @submitRegisterClaims @negative @wip
  Scenario Outline: Try to Register a new member with already existing [<key>] - Negative
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I set random value with format {<format>} to field {<key>} inside Request Body
    Then I register a new member via VIAM API
    And the field {status} has the value {Bad request}
    And the field {code} has the value {400}
    And the field {data} has the value {Identificator is already taken}
    Examples:
      | key         | format               |
      | email       | xxxxxxxxxxx@yyyy.zzz |
      | phonenumber | 1111111111           |

  @submitRegisterClaims @negative
  Scenario: Try to register without providing a publickey - Negative
   #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I delete the headers
      | publicKey |
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {Not provided public key}
    And the field {code} has the value {400}

  @submitRegisterClaims @negative
  Scenario: Try to add register claims without provided correct confirmation code - Negative
  #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128361} via VIAM API
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {Entity is not activated}
    And the field {code} has the value {400}