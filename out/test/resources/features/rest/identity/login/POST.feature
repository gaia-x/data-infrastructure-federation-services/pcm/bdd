#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/login
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - login POST
  This call logins a member

  Background:
    Given we are testing the VIAM Api

  @login
  Scenario: Login a new member with previousaddeddevice mode - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.Session} is present and not empty
    And the field {$.data.Uuid} is present and not empty

  @login
  Scenario: Login a new member with newdevice mode - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.QrCode} is present and not empty
    And the field {$.data.ActionID} is present and not empty

  @login @addNewDevice
  Scenario: Create new device and then login - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
#Then login with the new device
    Given I login member with mode fromanotherauthenticateddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully authenticated}

  @login @negative
  Scenario: Try to Login a user that did not confirm the privacy - Negative
#Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Identity hasn't finished registration and was deleted}

  @login @negative
  Scenario: Try to Login a user that did not add the identity info - Negative
#Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Identity hasn't finished registration and was deleted}

  @login @negative
  Scenario: Try to Login a user that did not send confirmation code - Negative
#Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Identity hasn't finished registration and was deleted}

  @login @negative
  Scenario: Login with new device and use that qrCode and actionID to call login with fromanotherauthenticateddevice - Negative
#Call login with mode newdevice
    Given I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.QrCode} is present and not empty
    And the field {$.data.ActionID} is present and not empty
    Then I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {QrCode}
    Then I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {ActionID}
#Then I call login with mode fromanotherauthenticateddevice
    Given I clear the request body
    Then I load object with key {QrCode} from DataContainer into currentRequest Body with key {code}
    Then I load object with key {ActionID} from DataContainer into currentRequest Body with key {actionID}
    Given I login member with mode fromanotherauthenticateddevice via VIAM API
    Then the field {status} has the value {Login not succesful}
    And the field {code} has the value {400}
    And the field {data} has the value {Action function arguments count mismatch}

  @destroyKeysForDevice @confirmAction @login @wip
  Scenario: Try to login with destroyed device key - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}
#I call to destroy the current publicKey to the device
    Given I set the following request body {{}}
    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
    Then I destroy a key for a device via VIAM API
    And the field {status} has the value {OK}
#Then I confirm the action
    Given I set the following request body {{}}
    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I confirm the current action via VIAM API
    And the field {status} has the value {OK}
#Try to login the destroyed device
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {redis: nil}

  @login @negative
  Scenario: Try to login with a suspended device
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Suspend the device
    And I suspend the current device via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {{}}
#Login with previousaddeddevice
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Device suspended}

  @login @negative
  Scenario: Try to login with a revoked device
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Revoke the device
    And I revoke the current device via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {{}}
#Login with previousaddeddevice
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Device revoked}

  @login
  Scenario: Login with device that was suspended and resumed
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with previousaddeddevice
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {Uuid}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
    And the field {$..data[0].Status} has the value {[0]}
    And the field {$..data[1].Status} has the value {[3]}
#authorize the new device
    And I authorize the current new device via VIAM API
    Then the field {status} has the value {OK}
#Suspend the Device
    And I suspend the current device via VIAM API
    And the field {status} has the value {OK}
#Load the second public key
    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#login with previousaddeddevice
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {Error while login}
    And the field {code} has the value {400}
    And the field {data} has the value {Device suspended}
#Load the first public key
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#Resume the current device
    And I resume the current device via VIAM API
    And the field {status} has the value {OK}
#Load the second public key
    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#login with previousaddeddevice
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.Uuid} has the value stored in DataContainer with key {Uuid}
