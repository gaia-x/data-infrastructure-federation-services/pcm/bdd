#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/confirmNewDevice
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - confirmNewDevice POST
  This endpoints is for confirmation of new desktop devices. It confirms the action for
  adding new device by already logged in device via QR Code and ActionID

  Background:
    Given we are testing the VIAM Api

  @confirmNewDevice
  Scenario: Confirm a new device - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}

  @confirmNewDevice @negative
  Scenario: Try to Confirm a new device with the new device publicKey - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I clear the request body
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {No authentication values}
    And the field {code} has the value {400}

  @confirmNewDevice @negative
  Scenario: Try add a new device and confirm it by having the same publicKey as the original - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {Action not succesful}
    And the field {code} has the value {400}
    And the field {data} has the value {Can not authenticate action from same device}

  @confirmNewDevice @negative
  Scenario Outline: Try to confirm a new device with missing auth header [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Try to Confirm the new device
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I delete the headers
      | <header> |
    Given I confirm the current new device via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |
