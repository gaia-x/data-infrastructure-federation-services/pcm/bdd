#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/logout
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - logout POST
  This call is to logout the current member

  Background:
    Given we are testing the VIAM Api

  @logout
  Scenario: Logout a new member - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Logout
    Given I clear the request body
    Then I call POST /logout via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @logout @negative
  Scenario: Try to Logout an already logged out member - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Logout
    Given I clear the request body
    Then I call POST /logout via VIAM API
    And the field {status} has the value {OK}
#Try to Logout an already logged out member
    Then I call POST /logout via VIAM API
    And the field {status} has the value {Bad session}
    And the field {code} has the value {400}

  @logout @negative
  Scenario Outline: Try to Logout with missing authorization [<header>] - negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Logout
    Given I clear the request body
    And I delete the headers
      | <header> |
    Then I call POST /logout via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header |
      | token  |
      | uuid   |