#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/submitIdentificator
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - confirmIdentificator POST
  This call registers a new member

  Background:
    Given we are testing the VIAM Api

  @confirmIdentificator
  Scenario: Submit a new email Identificator - Positive
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @confirmIdentificator
  Scenario: Submit a new phone Identificator - Positive
    Given I submit a new identificator with random phoneNumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}