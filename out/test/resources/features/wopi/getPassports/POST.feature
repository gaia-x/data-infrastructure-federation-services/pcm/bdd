#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#WOPI http://localhost:8787/getPassports
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@wopi @all
Feature: WOPI - getPassports POST
  Get all passports that entity has via WOPI

  Background:
    Given we are testing the VIAM Api

  @getPassportsWopi
  Scenario: Get All passports that an entity has via WOPI - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
##Then I link the passport to the entity
    Given I clear the request body
    Then I get the guarded entity via the VIAM API
    And the field {status} has the value {OK}
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
#Get Passports via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {ok}
    And the field {code} has the value {200}
    And the field {$..PassportUUID} is containing the value stored in DataContainer with key {firstPassportUUID}
    And the field {$.data[0].UUID} is containing the value stored in DataContainer with key {personUUID1}
    And the field {$.data[0].AccessToken} is present and not empty

  @getPassportsWopi @negative
  Scenario Outline: Try to Get All passports that an entity has via WOPI with missing auth header [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
##Then I link the passport to the entity
    Given I clear the request body
    Then I get the guarded entity via the VIAM API
    And the field {status} has the value {OK}
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
#Get Passports via WOPI
    Given we are testing with the current session the WOPI
    Then I delete the headers
      | <header> |
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {Bad Request}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | header    | data                        |
      | uuid      | Uuid is not specified       |
      | token     | Token is not specified      |
      | publicKey | Public Key is not specified |

  @getPassportsWopi @negative @bug-wopi-4
  Scenario Outline: Try to Get All passports that an entity has via WOPI with invalid auth header [<header>] & [<value>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
##Then I link the passport to the entity
    Given I clear the request body
    Then I get the guarded entity via the VIAM API
    And the field {status} has the value {OK}
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
#Get Passports via WOPI
    Given we are testing with the current session the WOPI
    Then I set the headers
      | <header> | <value> |
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {Bad Request}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | header    | value | data                        |
     # | uuid      | dsada | Uuid is not correct       |
      | uuid      |       | Uuid is not specified       |
    #  | token     | dasda | Token is not correct      |
      | token     |       | Token is not specified      |
     # | publicKey | dsaas | Public Key is not correct |
      | publicKey |       | Public Key is not specified |

  @getPassportsWopi @negative
  Scenario Outline: Try to Get All passports that an entity has with invalid fileId [<fileId>] via WOPI - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
##Then I link the passport to the entity
    Given I clear the request body
    Then I get the guarded entity via the VIAM API
    And the field {status} has the value {OK}
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
#Get Passports via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {<fileId>} via WOPI
    And the field {status} has the value {Bad Request}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | fileId | data                     |
      |        | File ID is not specified |