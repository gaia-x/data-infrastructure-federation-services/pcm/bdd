$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/selenium/dashboard/DeviceManagement.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Copyright (c) 2018 Vereign AG [https://www.vereign.com]"
    },
    {
      "line": 2,
      "value": "#"
    },
    {
      "line": 3,
      "value": "#This is free software: you can redistribute it and/or modify"
    },
    {
      "line": 4,
      "value": "#it under the terms of the GNU Affero General Public License as"
    },
    {
      "line": 5,
      "value": "#published by the Free Software Foundation, either version 3 of the"
    },
    {
      "line": 6,
      "value": "#License, or (at your option) any later version."
    },
    {
      "line": 7,
      "value": "#"
    },
    {
      "line": 8,
      "value": "#This program is distributed in the hope that it will be useful,"
    },
    {
      "line": 9,
      "value": "#but WITHOUT ANY WARRANTY; without even the implied warranty of"
    },
    {
      "line": 10,
      "value": "#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
    },
    {
      "line": 11,
      "value": "#GNU Affero General Public License for more details."
    },
    {
      "line": 12,
      "value": "#"
    },
    {
      "line": 13,
      "value": "#You should have received a copy of the GNU Affero General Public License"
    },
    {
      "line": 14,
      "value": "#along with this program. If not, see \u003chttp://www.gnu.org/licenses/\u003e."
    },
    {
      "line": 15,
      "value": "#Author: Boris Dimitrov boris.dimitrov@vereign.com"
    }
  ],
  "line": 18,
  "name": "Dashboard - Device Information",
  "description": "",
  "id": "dashboard---device-information",
  "keyword": "Feature",
  "tags": [
    {
      "line": 17,
      "name": "@selenium"
    },
    {
      "line": 17,
      "name": "@all"
    },
    {
      "line": 17,
      "name": "@dashboard"
    },
    {
      "line": 17,
      "name": "@test"
    }
  ]
});
formatter.scenarioOutline({
  "line": 24,
  "name": "Register a new user and validate his current device information",
  "description": "",
  "id": "dashboard---device-information;register-a-new-user-and-validate-his-current-device-information",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 23,
      "name": "@deviceManager"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "I open Chrome browser and navigate to vereign login page",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "I register a new user with email via VIAM API",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I login with the registered via the VIAM API user into dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "User click on close tour button",
  "keyword": "When "
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Logged in"
    }
  ],
  "line": 30,
  "name": "User click on device manager button",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 31,
      "value": "#Validate Devices"
    }
  ],
  "line": 32,
  "name": "Validate \"1\" devices are presented",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Validate Device name field has value \"Linux x86_64 | Chrome |\" after registration",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "Validate Device type field has value \"Linux x86_64\"",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Validate Device ID field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Validate Browser field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "Validate Created On field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "Validate Last Accessed field is not empty",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 39,
      "value": "#Validate QR Code labels"
    }
  ],
  "line": 40,
  "name": "User click on Add new device button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "Validate QR code info label text \"\u003cexpected text\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "Validate QR code is presented and close it",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.examples({
  "line": 45,
  "name": "",
  "description": "",
  "id": "dashboard---device-information;register-a-new-user-and-validate-his-current-device-information;",
  "rows": [
    {
      "cells": [
        "expected text"
      ],
      "line": 46,
      "id": "dashboard---device-information;register-a-new-user-and-validate-his-current-device-information;;1"
    },
    {
      "cells": [
        "Please open app.vereign.com on your device browser and click Authenticate new mobile device. You can then scan the code to login."
      ],
      "line": 47,
      "id": "dashboard---device-information;register-a-new-user-and-validate-his-current-device-information;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 256261031,
  "status": "passed"
});
formatter.background({
  "line": 20,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 21,
  "name": "we are testing the VIAM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_VIAM_api()"
});
formatter.result({
  "duration": 343519982,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Register a new user and validate his current device information",
  "description": "",
  "id": "dashboard---device-information;register-a-new-user-and-validate-his-current-device-information;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 17,
      "name": "@dashboard"
    },
    {
      "line": 23,
      "name": "@deviceManager"
    },
    {
      "line": 17,
      "name": "@selenium"
    },
    {
      "line": 17,
      "name": "@test"
    },
    {
      "line": 17,
      "name": "@all"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "I open Chrome browser and navigate to vereign login page",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "I register a new user with email via VIAM API",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I login with the registered via the VIAM API user into dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "User click on close tour button",
  "keyword": "When "
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Logged in"
    }
  ],
  "line": 30,
  "name": "User click on device manager button",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 31,
      "value": "#Validate Devices"
    }
  ],
  "line": 32,
  "name": "Validate \"1\" devices are presented",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Validate Device name field has value \"Linux x86_64 | Chrome |\" after registration",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "Validate Device type field has value \"Linux x86_64\"",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Validate Device ID field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Validate Browser field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "Validate Created On field is not empty",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "Validate Last Accessed field is not empty",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 39,
      "value": "#Validate QR Code labels"
    }
  ],
  "line": 40,
  "name": "User click on Add new device button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "Validate QR code info label text \"Please open app.vereign.com on your device browser and click Authenticate new mobile device. You can then scan the code to login.\"",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "Validate QR code is presented and close it",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.user_is_on_the_vereign_login_page()"
});
formatter.result({
  "duration": 3475210366,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.iRegisterANewUserWithEmailViaVIAMAPI()"
});
formatter.result({
  "duration": 8893683084,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.iLoginTheRegisteredViaTheVIAMAPIUserIntoDashboard()"
});
formatter.result({
  "duration": 9202634854,
  "status": "passed"
});
formatter.match({
  "location": "NavigationStepDefinitions.userCloseTheTour()"
});
formatter.result({
  "duration": 77697409,
  "status": "passed"
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.clickDeviceManagerButton()"
});
formatter.result({
  "duration": 163765135,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 10
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceIsPresented(String)"
});
formatter.result({
  "duration": 537111344,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Linux x86_64 | Chrome |",
      "offset": 38
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceNameFieldAfterRegistration(String)"
});
formatter.result({
  "duration": 126151388,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "type",
      "offset": 16
    },
    {
      "val": "Linux x86_64",
      "offset": 38
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceNameFieldHasValue(String,String)"
});
formatter.result({
  "duration": 103009394,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Device ID",
      "offset": 9
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceIDFieldIsNotEmpty(String)"
});
formatter.result({
  "duration": 91303461,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Browser",
      "offset": 9
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceIDFieldIsNotEmpty(String)"
});
formatter.result({
  "duration": 95078323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Created On",
      "offset": 9
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceIDFieldIsNotEmpty(String)"
});
formatter.result({
  "duration": 124616347,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Last Accessed",
      "offset": 9
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceIDFieldIsNotEmpty(String)"
});
formatter.result({
  "duration": 124541992,
  "status": "passed"
});
formatter.match({
  "location": "DeviceManagerStepDefinitions.userClickOnAddNewDeviceButton()"
});
formatter.result({
  "duration": 131774076,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Please open app.vereign.com on your device browser and click Authenticate new mobile device. You can then scan the code to login.",
      "offset": 34
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceManagementMainLabelText(String)"
});
formatter.result({
  "duration": 109705105,
  "status": "passed"
});
formatter.match({
  "location": "DeviceManagerStepDefinitions.validateQRCodeIsPresented()"
});
formatter.result({
  "duration": 406073475,
  "status": "passed"
});
formatter.match({
  "location": "SeleniumStepDefinitions.iCloseTheBrowser()"
});
formatter.result({
  "duration": 91558914,
  "status": "passed"
});
formatter.after({
  "duration": 100172,
  "status": "passed"
});
formatter.after({
  "duration": 851893,
  "status": "passed"
});
formatter.before({
  "duration": 1619385,
  "status": "passed"
});
formatter.background({
  "line": 20,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 21,
  "name": "we are testing the VIAM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_VIAM_api()"
});
formatter.result({
  "duration": 212822,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Rename and revoke device",
  "description": "",
  "id": "dashboard---device-information;rename-and-revoke-device",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 49,
      "name": "@deviceManager"
    }
  ]
});
formatter.step({
  "line": 51,
  "name": "I open Chrome browser and navigate to vereign login page",
  "keyword": "Given "
});
formatter.step({
  "line": 52,
  "name": "I register a new user with email via VIAM API",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "I login with the registered via the VIAM API user into dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "User click on close tour button",
  "keyword": "When "
});
formatter.step({
  "comments": [
    {
      "line": 55,
      "value": "#Logged in"
    }
  ],
  "line": 56,
  "name": "User click on device manager button",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 57,
      "value": "#Rename Device"
    }
  ],
  "line": 58,
  "name": "User hovers over the options button on the first device",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "User click on Rename device button",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "User populates the new device name \"Device edited\" and Save",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "User click on Profiles tab",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "User click on device manager button",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "Validate Device name field has value \"Device edited\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 64,
      "value": "#Revoke Device"
    }
  ],
  "line": 65,
  "name": "User hovers over the options button on the first device",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "User click on Revoke device button",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "Validate user is navigated to login page",
  "keyword": "Then "
});
formatter.step({
  "line": 68,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.user_is_on_the_vereign_login_page()"
});
formatter.result({
  "duration": 1306935979,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.iRegisterANewUserWithEmailViaVIAMAPI()"
});
formatter.result({
  "duration": 3988496544,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.iLoginTheRegisteredViaTheVIAMAPIUserIntoDashboard()"
});
formatter.result({
  "duration": 10190107111,
  "status": "passed"
});
formatter.match({
  "location": "NavigationStepDefinitions.userCloseTheTour()"
});
formatter.result({
  "duration": 82414172,
  "status": "passed"
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.clickDeviceManagerButton()"
});
formatter.result({
  "duration": 145657774,
  "status": "passed"
});
formatter.match({
  "location": "DeviceManagerStepDefinitions.userHoverOverTheOptionsButtonOnTheFirstDevice()"
});
formatter.result({
  "duration": 1292322948,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Rename",
      "offset": 14
    }
  ],
  "location": "DeviceManagerStepDefinitions.userClickOnRenameDeviceButton(String)"
});
formatter.result({
  "duration": 171708181,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Device edited",
      "offset": 36
    }
  ],
  "location": "DeviceManagerStepDefinitions.userPopulatesTheNewDeviceName(String)"
});
formatter.result({
  "duration": 268658104,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Profiles",
      "offset": 14
    }
  ],
  "location": "NavigationStepDefinitions.userClickOnIdentityButton(String)"
});
formatter.result({
  "duration": 249078499,
  "status": "passed"
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.clickDeviceManagerButton()"
});
formatter.result({
  "duration": 180579113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "name",
      "offset": 16
    },
    {
      "val": "Device edited",
      "offset": 38
    }
  ],
  "location": "DeviceManagerStepDefinitions.validateDeviceNameFieldHasValue(String,String)"
});
formatter.result({
  "duration": 645022611,
  "status": "passed"
});
formatter.match({
  "location": "DeviceManagerStepDefinitions.userHoverOverTheOptionsButtonOnTheFirstDevice()"
});
formatter.result({
  "duration": 188126775,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Revoke",
      "offset": 14
    }
  ],
  "location": "DeviceManagerStepDefinitions.userClickOnRenameDeviceButton(String)"
});
formatter.result({
  "duration": 155859088,
  "status": "passed"
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.validateUserIsNavigatedToLoginPage()"
});
formatter.result({
  "duration": 10169048656,
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for url to be \"https://integration.vereign.com/login\". Current url: \"https://integration.vereign.com/devicemanager\" (tried for 10 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027vereign-e4802\u0027, ip: \u0027192.168.88.243\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00273.10.0-957.21.2.el7.x86_64\u0027, java.version: \u00271.8.0_212\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 75.0.3770.90, chrome: {chromedriverVersion: 75.0.3770.90 (a6dcaf7e3ec6f..., userDataDir: /tmp/.com.google.Chrome.3g0KMs}, goog:chromeOptions: {debuggerAddress: localhost:46678}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: 57201799666bdebf93b163304249b993\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:95)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:272)\n\tat selenium.dashboard.DashboardPage.validateLoginPageIsPresented(DashboardPage.java:74)\n\tat api.test.selenium.dashboard.GeneralDashboardStepDefinitions.validateUserIsNavigatedToLoginPage(GeneralDashboardStepDefinitions.java:166)\n\tat ✽.Then Validate user is navigated to login page(features/selenium/dashboard/DeviceManagement.feature:67)\n",
  "status": "failed"
});
formatter.match({
  "location": "SeleniumStepDefinitions.iCloseTheBrowser()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("Printing last request and response:");
formatter.write("---- Request ----\nMethod(verb): POST\nPath:         /identity/agreeOnRegistration\nHeaders:      \n  publicKey:KVHlFFsiZuWrjTeuZWBerfzs6thliyFt+RdUhR7Unr+bdbnHt4WgSiMqAB5alrn0v/whjB6mMCdFp6IF6ACBHAfNP4aktpRkNIKZTHxTI8CKuaVJ7/2vASGsLw6n3aKG3W8Y/YnY66aS3dV0VWTq9SsVI6Zrfn390/W7ApfZB/v1R3Dx9jPK1gYXLKIJiqVyff8TeFVuVuLzmEHkdif6NYBQ4IKqQniOkkIB16s6XfX+a1I3wY+27o+wgQFjVTxNf6G2kUpeGcdkay1yFv3cSZdjbiPrICxVsdN3MQrd2ecIdFRsBE+HvYLjjHxWZ4rGmLxsLD8zCmkW2mliRCuDow\u003d\u003d\nBody:\n\nParams:       none\nPath parameters:none\nQueryParams:  none\nVersion:      \n--------------------------------------------------------------");
formatter.write("----Response----\nStatus code: 200\nHeaders:\n  Server:nginx\n  Access-Control-Allow-Origin:http://localhost:9000\n  Connection:keep-alive\n  Content-Length:39\n  Date:Thu, 29 Aug 2019 11:32:31 GMT\n  Access-Control-Allow-Headers:*\n  Content-Type:application/json\nBody:\n{\n  \"status\": \"OK\",\n  \"code\": \"200\",\n  \"data\": {}\n}\n\n-------------------------------------------------------------");
formatter.after({
  "duration": 82084251,
  "status": "passed"
});
formatter.after({
  "duration": 68220,
  "status": "passed"
});
formatter.uri("features/selenium/dashboard/Plugins.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Copyright (c) 2018 Vereign AG [https://www.vereign.com]"
    },
    {
      "line": 2,
      "value": "#"
    },
    {
      "line": 3,
      "value": "#This is free software: you can redistribute it and/or modify"
    },
    {
      "line": 4,
      "value": "#it under the terms of the GNU Affero General Public License as"
    },
    {
      "line": 5,
      "value": "#published by the Free Software Foundation, either version 3 of the"
    },
    {
      "line": 6,
      "value": "#License, or (at your option) any later version."
    },
    {
      "line": 7,
      "value": "#"
    },
    {
      "line": 8,
      "value": "#This program is distributed in the hope that it will be useful,"
    },
    {
      "line": 9,
      "value": "#but WITHOUT ANY WARRANTY; without even the implied warranty of"
    },
    {
      "line": 10,
      "value": "#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
    },
    {
      "line": 11,
      "value": "#GNU Affero General Public License for more details."
    },
    {
      "line": 12,
      "value": "#"
    },
    {
      "line": 13,
      "value": "#You should have received a copy of the GNU Affero General Public License"
    },
    {
      "line": 14,
      "value": "#along with this program. If not, see \u003chttp://www.gnu.org/licenses/\u003e."
    },
    {
      "line": 15,
      "value": "#Author: Boris Dimitrov boris.dimitrov@vereign.com"
    }
  ],
  "line": 18,
  "name": "Dashboard - Plugins",
  "description": "",
  "id": "dashboard---plugins",
  "keyword": "Feature",
  "tags": [
    {
      "line": 17,
      "name": "@selenium"
    },
    {
      "line": 17,
      "name": "@all"
    },
    {
      "line": 17,
      "name": "@dashboard"
    },
    {
      "line": 17,
      "name": "@test"
    }
  ]
});
formatter.before({
  "duration": 1221248,
  "status": "passed"
});
formatter.background({
  "line": 20,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 21,
  "name": "we are testing the VIAM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_VIAM_api()"
});
formatter.result({
  "duration": 752339,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Validate Plugins Page",
  "description": "",
  "id": "dashboard---plugins;validate-plugins-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 23,
      "name": "@plugins"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "I open Chrome browser and navigate to vereign login page",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "I register a new user with email via VIAM API",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I login with the registered via the VIAM API user into dashboard",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 28,
      "value": "#Logged in"
    }
  ],
  "line": 29,
  "name": "User click on close tour button",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "User click on Plugins tab",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 31,
      "value": "#Privacy Policy and Beta Agreement validation"
    }
  ],
  "line": 32,
  "name": "Privacy policy and Beta Participation Agreement Links have the correct text and href",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 33,
      "value": "#Chrome Plugin validation"
    }
  ],
  "line": 34,
  "name": "Validate Chrome Plugin have title \"Chrome extension\"",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Validate Chrome Plugin have subtitle \"Extend Vereign into your Gmail and Drive accounts\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 36,
      "value": "#Collabora plugin validation"
    }
  ],
  "line": 37,
  "name": "Validate Collabora Plugin have title \"Collabora plugin\"",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "Validate Collabora Plugin have subtitle \"Included by default in the leading open source collaboration suite\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 39,
      "value": "#Roundcube Plugin validation"
    }
  ],
  "line": 40,
  "name": "Validate Roundcube Plugin have title \"Roundcube plugin\"",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "Validate Roundcube Plugin have subtitle \"Add Vereign to your favorite open source webmail\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 42,
      "value": "#Chrome Plugin download button validation"
    }
  ],
  "line": 43,
  "name": "User click on visit Chrome",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "Title of the page is \"Vereign Beta for GMail - Chrome Web Store\"",
  "keyword": "Then "
});
formatter.step({
  "line": 45,
  "name": "Validate Add to Chrome button is presented",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 46,
      "value": "#Collabora Plugin download button validation"
    }
  ],
  "line": 47,
  "name": "User click on visit Collabora",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "Title of the page is \"Collabora Online Development Edition (CODE) - Collabora Productivity\"",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.user_is_on_the_vereign_login_page()"
});
formatter.result({
  "duration": 1357929784,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.iRegisterANewUserWithEmailViaVIAMAPI()"
});
formatter.result({
  "duration": 6334808653,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.iLoginTheRegisteredViaTheVIAMAPIUserIntoDashboard()"
});
formatter.result({
  "duration": 10127083441,
  "status": "passed"
});
formatter.match({
  "location": "NavigationStepDefinitions.userCloseTheTour()"
});
formatter.result({
  "duration": 92183491,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Plugins",
      "offset": 14
    }
  ],
  "location": "NavigationStepDefinitions.userClickOnIdentityButton(String)"
});
formatter.result({
  "duration": 280702548,
  "status": "passed"
});
formatter.match({
  "location": "GeneralDashboardStepDefinitions.privacyPolicyAndBetaParticipationAgreementLinksHaveTheCorrectTextAndHref()"
});
formatter.result({
  "duration": 1557841862,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 9
    },
    {
      "val": "title",
      "offset": 28
    },
    {
      "val": "Chrome extension",
      "offset": 35
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 39862848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 9
    },
    {
      "val": "subtitle",
      "offset": 28
    },
    {
      "val": "Extend Vereign into your Gmail and Drive accounts",
      "offset": 38
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 31211900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Collabora",
      "offset": 9
    },
    {
      "val": "title",
      "offset": 31
    },
    {
      "val": "Collabora plugin",
      "offset": 38
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 84241392,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Collabora",
      "offset": 9
    },
    {
      "val": "subtitle",
      "offset": 31
    },
    {
      "val": "Included by default in the leading open source collaboration suite",
      "offset": 41
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 118123153,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Roundcube",
      "offset": 9
    },
    {
      "val": "title",
      "offset": 31
    },
    {
      "val": "Roundcube plugin",
      "offset": 38
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 28555016,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Roundcube",
      "offset": 9
    },
    {
      "val": "subtitle",
      "offset": 31
    },
    {
      "val": "Add Vereign to your favorite open source webmail",
      "offset": 41
    }
  ],
  "location": "PluginsStepDefinitions.validateChromePluginHaveTitle(String,String,String)"
});
formatter.result({
  "duration": 29685228,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 20
    }
  ],
  "location": "PluginsStepDefinitions.userClickOnDownloadChromePluginButton(String)"
});
formatter.result({
  "duration": 821033725,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "is",
      "offset": 18
    },
    {
      "val": "Vereign Beta for GMail - Chrome Web Store",
      "offset": 22
    }
  ],
  "location": "GeneralDashboardStepDefinitions.title_of_the_page_is(String,String)"
});
formatter.result({
  "duration": 606886767,
  "status": "passed"
});
formatter.match({
  "location": "PluginsStepDefinitions.validateAddToChromeButtonIsPresented()"
});
formatter.result({
  "duration": 401256193,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Collabora",
      "offset": 20
    }
  ],
  "location": "PluginsStepDefinitions.userClickOnDownloadChromePluginButton(String)"
});
formatter.result({
  "duration": 2453976077,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "is",
      "offset": 18
    },
    {
      "val": "Collabora Online Development Edition (CODE) - Collabora Productivity",
      "offset": 22
    }
  ],
  "location": "GeneralDashboardStepDefinitions.title_of_the_page_is(String,String)"
});
formatter.result({
  "duration": 7439418,
  "status": "passed"
});
formatter.match({
  "location": "SeleniumStepDefinitions.iCloseTheBrowser()"
});
formatter.result({
  "duration": 121240772,
  "status": "passed"
});
formatter.after({
  "duration": 80356,
  "status": "passed"
});
formatter.after({
  "duration": 51354,
  "status": "passed"
});
});