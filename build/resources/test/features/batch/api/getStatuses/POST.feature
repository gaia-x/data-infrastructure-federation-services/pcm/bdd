#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost/api/getStatuses
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @batch @all
Feature: API - getStatuses POST
  Get the previously added Batches

  Background:
    Given we are testing the VIAM Api

  @getStatuses @test
  Scenario: Send a batch request and then fetch it with getStatuses - Positive
#Send Batch
    Given I load the REST request {Batch.json} with profile {successful_batch}
    Given I send a new random batch request via API
    And the status code should be {200}
#Get Statuses
    Then I wait for {60000} mseconds
    Given I clear the request body
    Then I call getStatuses with the current sealKey via API
    And the status code should be {200}
    And the field {$.status} has the value {OK}
    And the field {$.code} has the value {200}
    And the response is valid according to the {Batch_GetStatuses_schema.json} REST schema
    And I assert that getStatus status is the same as the sendBatch value via API

  @getStatuses @negative
  Scenario Outline:  Try to call getStatuses with invalid request - <profile> - Negative
#Get Statuses
    Given I clear the request body
    Given I load the REST request {Batch.json} with profile {<profile>}
    Then I call getStatuses request via API
    And the status code should be {400}
    And the field {$.message} has the value {<msg>}
    Examples:
      | profile                   | msg                     |
      | getStatuses_missing_key   | sealID (key) is missing |
      | getStatuses_missing_value | sealID (key) is missing |
      | getStatuses_empty         | sealID (key) is missing |

  @sendbatch
  Scenario: Send a batch requests with 2 objects and then get them - Positive
#Send Batch
    Given I load the REST request {Batch.json} with profile {successful_batch}
    Then I send a new random batch request with 2 objects via API
    And the status code should be {200}
    And the field {message} has the value {batch is queued}
#Get Statuses of seal key 1
    Then I wait for {60000} mseconds
    Given I clear the request body
    Then I call getStatuses with the current sealKey via API
    And the status code should be {200}
    And the response is valid according to the {Batch_GetStatuses_schema.json} REST schema
    And I assert that getStatus status is the same as the sendBatch value via API
#Get Statuses of seal key 2
    Given I clear the request body
    Then I call getStatuses with the current sealKey2 via API
    And the status code should be {200}
    And the response is valid according to the {Batch_GetStatuses_schema.json} REST schema
    And I assert that getStatus status is the same as the sendBatch value via API

  @sendbatch
  Scenario: Send 2 separate batch requests for the same key and then get them - Positive
#Send Batch
    Given I load the REST request {Batch.json} with profile {successful_batch}
    Given I send a new random batch request via API
    And the status code should be {200}
    Then I send a new batch request via API
    And the status code should be {200}
    And the field {message} has the value {batch is queued}
#Get Statuses
    Then I wait for {120000} mseconds
    Given I clear the request body
    Then I call getStatuses with the current sealKey via API
    And the status code should be {200}
    And the field {$..Status} contains {2} elements