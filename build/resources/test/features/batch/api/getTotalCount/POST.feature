#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost/api/sendbatch
#Author: Georgi Michev georgi.michev@vereign.como

@rest @batch @all
Feature: API - getTotalCount POST
  Retrieve counters for processed seal statuses and batches

  Background:
    Given we are testing the VIAM Api

  @getTotalCount
  Scenario: Send request with empty body - Positive
  #Send Request
  Given I send a total count request via API
    And the status code should be {200}
    And the response is valid according to the {Batch_GetTotalCount_schema.json} REST schema
    And the field {data} is present and not empty
    And the field {data} is not equal to the value {0}
    And the field {receivedStatus} is present and not empty
    And the field {receivedStatus} is not equal to the value {0}
    And the field {storedStatus} is present and not empty
    And the field {storedStatus} is not equal to the value {0}
    And the field {anchoredStatus} is present and not empty
    And the field {anchoredStatus} is not equal to the value {0}
    And the field {anchoredBatches} is present and not empty
    And the field {anchoredBatches} is not equal to the value {0}
