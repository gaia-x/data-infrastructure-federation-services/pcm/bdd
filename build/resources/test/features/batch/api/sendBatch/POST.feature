#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost/api/sendbatch
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @batch @all
Feature: API - sendBatch POST
  This all sends batch request

  Background:
    Given we are testing the VIAM Api

  @sendbatch
  Scenario: Send a new random batch - Positive
#Send Batch
    Given I load the REST request {Batch.json} with profile {successful_batch}
    Given I send a new random batch request via API
    And the status code should be {200}
    And the field {message} has the value {batch is queued}

  @sendbatch
  Scenario: Send 2 batch requests for the same key - Positive
#Send Batch
    Given I load the REST request {Batch.json} with profile {successful_batch}
    Given I send a new random batch request via API
    And the status code should be {200}
    Then I send a new batch request via API
    And the status code should be {200}
    And the field {message} has the value {batch is queued}

  @sendbatch @negative
  Scenario Outline: Try to send invalid batch requests for sendBatch - <profile> - Negative
#Send Batch
    Given I load the REST request {Batch.json} with profile {<profile>}
    Given I send a new batch request via API
    And the status code should be {<code>}
    And the field {message} has the value {<message>}
    And the field {kind} has the value {<kind>}
    And the field {id} is present and not empty
    Examples:
      | profile          | message                      | code | kind |
      | missing_batch    | no status entries to process | 400  | 1    |
      | empty_seal_key   | invalid batch                | 400  | 1    |
      | empty_seal_value | invalid batch                | 400  | 1    |
      | missing_seal     | no status entries to process | 400  | 1    |
      | not_base64_value | invalid batch                | 400  | 1    |


